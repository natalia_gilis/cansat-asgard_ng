/*
 * This is a subclass of EEPROM_Bank completed with utility methods for testing only
 */

#pragma once
#include "EEPROM_Bank.h"

/** @ingroup CSPU_EEPROM
 *  @brief A COMPLETER
 *  
 *  DESCRIPTION A COMPLETER.
 */
class EEPROM_BankWithTools : public EEPROM_Bank {
public:
	EEPROM_BankWithTools(const unsigned int theMaintenancePeriodInSec=10);

	/* Utility method: write memory content in user readable format */
	VIRTUAL_FOR_TEST void printMemory() const{};
	/* Utility method: dump memory in hex format */
	VIRTUAL_FOR_TEST void hexDumpData() const{};
	/* Utility method: read header from chip */
	VIRTUAL_FOR_TEST void printHeaderFromChip() const;
	/* Utility method: read header from memory */
	VIRTUAL_FOR_TEST void printHeaderFromMemory() const;

    // Access header (for debugging
    NON_VIRTUAL const EEPROM_Header& getHeader() const {
      return EEPROM_BankWriter::getHeader();
    }

private:
	VIRTUAL_FOR_TEST void hexDumpBuffer(const byte * buffer, const unsigned int bufferSize) const;
};
