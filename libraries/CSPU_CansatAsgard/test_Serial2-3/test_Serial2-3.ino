/** @file test_Serial2-3.ino
 *  
 *  Test program for the Serial interfaces on SAMD21 (e.g. Adafruit Feather M0 Express) adn 
 *  SAMD51 (e.g. Adafruit ItsyBitsy M4)
 *  
 *  Inclusion of Serial2 on SAMD21 boards configures SerCom 1 to provide an additional Serial object (Serial2).
 *  Inclusion of Serial2 on SAMD51 boards configures SerCom 3 to provide an additional Serial object (Serial2).
 *  See RX and TX pins in header files.
 *  
 *  Serial3.h was an attempt to do the same with Sercom2, but does not work on Adafruit Feather M0 Express
 *  because Sercom2 is used for the flash, and the pins are not accessible. It should be
 *  applicable to other SAMD21 boards but not the Adafruit Feather M0 Express.
 *  
 *  Serial3 has not been tested on SAMD51.
 *  
 *  Inclusion of Serial2 and Serial3 on boards with additional UARTS (and consequently a core 
 *  including Serial2 and Serial3 objects already) is transparent (this is the case for 
 *  TMinus boards).
 *  
 *  Inclusion of Serial2 and Serial3 on board without additional UARTS (like Genuino Uno) triggers
 *  an error. On those boards, SoftwareSerial objects should be used instead.
 *  
 *  Connexions: SAMD21: D11 (Serial2 RX) to TX (Serial1 TX) and D10 (Serial2 TX) to RX (Serial1 RX)
 *              SAMD51: D11 (Serial2 RX) to TX (Serial1 TX) and D12 (Serial2 TX) to RX (Serial1 RX)
 *    
 */

//#define TEST_SERIAL3 Define to test Serial3 (not operational on Adafruit Feather M0 Express). 

// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#if  !defined(ARDUINO_ARCH_SAMD)
#  error "This sketch is only for boards with SAMD architecture"
#endif
 

 #include "DebugCSPU.h"
 #include "Serial2.h"

 #ifdef TEST_SERIAL3
 #include "Serial3.h" // This inclusion should trigger an error on Adafruit Feather M0 Express
 #endif 

 void transmitBetweenSerials(Stream & rx, Stream& tx) {
   int count = 0;
   const char* testStr="01234567890123456789";
   Serial << "Transfering string '" << testStr << "' with final CR" << ENDL; 
   
   int len=strlen(testStr);
   len++; // for CR character 

   // Empty reception buffer
   while (rx.available()) {
    rx.read();
   }
   
   tx << testStr << ENDL;
   auto start=millis();
   Serial << "Receiving: " ;
   while ((count <len)&& (millis()-start < 1000)) {
      if (rx.available()) {
        char c = rx.read();
        Serial << c;
        count++;
      }
      else {
        delay(10);
      }
   }
   if (count == len) {
      Serial << "Reception complete" << ENDL;
   }
   else Serial << "Error: reception time-out" << ENDL;
 }
 
 void setup() {
  DINIT(115200)
  Serial << "Serial port init OK" << ENDL; 

  Serial1.begin(115200);  
  Serial << "Serial1 init OK (RX, TX pins)" << ENDL; 

  Serial2.begin(115200);
  Serial << "Serial2 init OK (RX=" << Serial2.getRX() << ", TX=" << Serial2.getTX() << ")" << ENDL; 

  // Transmit from serial1 to serial 2 and conversely
  Serial << ENDL << "Transmitting from serial1 to serial2..." << ENDL;
  transmitBetweenSerials(Serial2, Serial1);
  Serial << "Transmitting from serial2 to serial1..." << ENDL;
  transmitBetweenSerials(Serial1, Serial2);
  
#ifdef TEST_SERIAL3
  Serial3.begin(115200);
  while(!Serial3) ;
  Serial << "Serial3 init OK (RX=" << Serial3.getRX() << ", TX=" << Serial3.getTX() << ")" << ENDL; 
#endif


  Serial << "Send character 'c' to start continuous test...." << ENDL;
  char c=0;
  while (c != 'c') {
    if (Serial.available()) {
      c= Serial.read();
      Serial << c << ENDL;
    }
    else delay(1);
  }
  
  Serial << "Now transmitting continuously..." << ENDL;

}

char i='a';
void loop() {
  if (i>'z') {
    i='a';
  }
  
  Serial2 << i++ << " ";
  Serial1 << i++ << " ";
  delay(1); // Just to allow the Serial interfaces to work.
  Serial << ENDL <<  "Received from Serial1: " ;
  while (Serial1.available()) {
    Serial << (char) Serial1.read();
   }
   
   Serial << ENDL <<  "Received from Serial2: " ;
   while (Serial2.available()) {
    Serial << (char) Serial2.read();
   }
  delay(100);

}
