/*
   HW_Subclass.cpp
*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#define DEBUG_CSPU
#define USE_ASSERTIONS
#include "DebugCSPU.h"
#include "HW_Subclass.h"

HW_Subclass::HW_Subclass() : CansatHW_Scanner() {
  DPRINTSLN(1, "HW_Subclass constructor called");
}

byte HW_Subclass::getExternalEEPROM_Address(const byte EEPROM_Number) const {
  switch (EEPROM_Number) {
    case 1:
      return 17;
      break;
    case 2:
      return 18;
      break;
    case 3:
      return 19;
      break;
    default:
      DASSERT(false);
  }
  return 0;
}

unsigned int HW_Subclass::getExternalEEPROM_LastAddress(const byte EEPROM_Number) const {
  switch (EEPROM_Number) {
    case 0:
      return (unsigned int)(32L * 1024L - 1L) ;
      break;
    case 1:
      return 16 * 1024 - 1;
      break;
    case 2:
      return 8 * 1024 - 1;
      break;
    default:
      DASSERT(false);
  }
  return 0;
}

byte HW_Subclass::getLED_PinNbr(LED_Type type) {
  switch (type) {
    case LED_Type::Storage:
      return 2;
      break;
    case LED_Type::Acquisition:
      return 5;
      break;
    default:
      return  getLED_PinNbr(type);
  }
  return 0;
}
