// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#define DEBUG_CSPU
#include <DebugCSPU.h>
#include "CansatRecord.h"

#define DBG 0
#define DBG_DIAGNOSTIC 1

// This is a pow() function that can be used in constexpr values. It return the powth power of num.
template <typename T>
constexpr T ipow(T num, uint8_t pow)
{
    return (pow >= sizeof(uint8_t)*8) ? 0 :
        pow == 0 ? 1 : num * ipow(num, pow-1);
}

/* The scaling factor used for binary coding of various data. For latitude/longitude, we need double precision */
static constexpr double LatLongAccuracyScalingFactor=ipow(10L, CansatRecord::NumDecimalPositions_GpsPosition);
static constexpr float TemperatureAccuracyScalingFactor=ipow(10L, CansatRecord::NumDecimalPositions_Temperature);
static constexpr float AltitudeAccuracyScalingFactor=ipow(10L, CansatRecord::NumDecimalPositions_Altitude);
static constexpr float PressureAccuracyScalingFactor=ipow(10L, CansatRecord::NumDecimalPositions_Pressure);
static constexpr float VelocityAccuracyScalingFactor=ipow(10L, CansatRecord::NumDecimalPositions_Velocity);

CansatRecord::CansatRecord () {
  CansatRecord::clear();
}

void CansatRecord::clear() {
  timestamp = 0;

  //A. GPS DATA
  newGPS_Measures = false;
  GPS_LatitudeDegrees = 0.00;
  GPS_LongitudeDegrees = 0.00;
  GPS_Altitude = 0.00;
#ifdef INCLUDE_GPS_VELOCITY
  GPS_VelocityKnots = 0.00;
  GPS_VelocityAngleDegrees = 0.00;
#endif

  //B. Primary Mission
  temperatureBMP = 0.00;
  pressure = 0.00;
  altitude = 0.00;
#ifdef INCLUDE_REFERENCE_ALTITUDE
  refAltitude= 0.00;
#endif
#ifdef INCLUDE_DESCENT_VELOCITY
  descentVelocity=0.00;
#endif
  temperatureThermistor1 = 0.00;
#ifdef INCLUDE_THERMISTOR2
  temperatureThermistor2 = 0.00;
#endif
#ifdef INCLUDE_THERMISTOR3
  temperatureThermistor3 = 0.00;
#endif

  // C. Secondary mission
  clearSecondaryMissionData();
}



void CansatRecord::printCSV(Stream& str, const float &f, bool finalSeparator, byte numDecimals) const {
  str.print(f, numDecimals);
  if (finalSeparator) {
    str << separator;
  }
}

void CansatRecord::printCSV(Stream& str, const bool b, bool finalSeparator) const {
  str.print((int) b);
  if (finalSeparator) {
    str << separator;
  }
}

uint16_t CansatRecord::getMaxCSV_Size() const {
	uint8_t numFloat=7;
#ifdef INCLUDE_GPS_VELOCIY
	numFloat+=2;
#endif
#ifdef INCLUDE_REFERENCE_ALTITUDE
	numFloat++;
#endif
#ifdef INCLUDE_DESCENT_VELOCITY
	numFloat++;
#endif
#ifdef INCLUDE_THERMISTOR2
	numFloat++;
#endif
#ifdef INCLUDE_THERMISTOR3
	numFloat++;
#endif
	constexpr uint8_t numBool=1;
	uint8_t numFields=numFloat+numBool+1;
	return 10   // timestamp
			+ numFloat*(5+CansatRecord::NumDecimalPositions_Default)
			+ numBool  // 1 character each
			+ numFields // separators
			+ getSecondaryMissionMaxCSV_Size();
}

uint16_t CansatRecord::getCSV_HeaderSize() const {
	uint16_t numChars=100;
#ifdef INCLUDE_GPS_VELOCITY
	numChars+=43;
#endif
#ifdef INCLUDE_THERMISTOR2
	numChars+=14;
#endif
#ifdef INCLUDE_THERMISTOR3
	numChars+=14;
#endif
#ifdef INCLUDE_DESCENT_VELOCITY
	numChars+=13;
#endif
#ifdef INCLUDE_REFERENCE_ALTITUDE
	numChars+=13;
#endif

	return numChars+getSecondaryMissionCSV_HeaderSize();
}

void CansatRecord::printCSV_ExceptTimestamp(Stream& str, DataSelector select, HeaderOrContent headerOrContent, bool finalSeparator) const {
  switch (headerOrContent) {
    case HeaderOrContent::Content:
      switch (select) {
        case DataSelector::GPS :
          printCSV(str, newGPS_Measures, true);
          printCSV(str, GPS_LatitudeDegrees, true, NumDecimalPositions_GpsPosition);
          printCSV(str, GPS_LongitudeDegrees, true,NumDecimalPositions_GpsPosition);
#ifdef INCLUDE_GPS_VELOCITY
          printCSV(str, GPS_Altitude, true, NumDecimalPositions_Altitude);
          printCSV(str, GPS_VelocityKnots, true);
          printCSV(str, GPS_VelocityAngleDegrees, false);
#else
          printCSV(str, GPS_Altitude, false, NumDecimalPositions_Altitude);
#endif
          break;

        case DataSelector::Primary :
          printCSV(str, temperatureBMP, true, NumDecimalPositions_Temperature);
          printCSV(str, pressure, true, NumDecimalPositions_Pressure);
          printCSV(str, altitude, true, NumDecimalPositions_Altitude);
#ifdef INCLUDE_REFERENCE_ALTITUDE
          printCSV(str, refAltitude, true, NumDecimalPositions_Altitude);
#endif
#ifdef INCLUDE_DESCENT_VELOCITY
          printCSV(str, descentVelocity, true);
#endif
          printCSV(str, temperatureThermistor1, false, NumDecimalPositions_Temperature);
#ifdef INCLUDE_THERMISTOR2
          str << separator;
          printCSV(str, temperatureThermistor2, false, NumDecimalPositions_Temperature);
#endif
#ifdef INCLUDE_THERMISTOR3
          str << separator;
          printCSV(str, temperatureThermistor3, false, NumDecimalPositions_Temperature);
#endif
          break;

        case DataSelector::Secondary :
          printCSV_SecondaryMissionData(str, false, false);
          break;
        case DataSelector::PrimarySecondary :
        	printCSV_ExceptTimestamp(str, DataSelector::Primary, HeaderOrContent::Content,false);
            printCSV_SecondaryMissionData(str, true, false);
          break;
        case DataSelector::All:
           	printCSV_ExceptTimestamp(str, DataSelector::GPS, HeaderOrContent::Content,true);
           	printCSV_ExceptTimestamp(str, DataSelector::Primary, HeaderOrContent::Content,false);
            printCSV_SecondaryMissionData(str, true, false);
          break;
        case DataSelector::AllExceptSecondary:
           	printCSV_ExceptTimestamp(str, DataSelector::GPS, HeaderOrContent::Content,true);
           	printCSV_ExceptTimestamp(str, DataSelector::Primary, HeaderOrContent::Content,false);
           	break;
        default :;
          DPRINT(DBG_DIAGNOSTIC, "Unexpected DataSelector value. Terminating Program");
          DASSERT (false);
          break;
      }
      if (finalSeparator) {
        str << separator;
      }
      break;


    case HeaderOrContent::Header:
      switch (select) {
        case DataSelector::GPS :
          str << F("GPS_Measures,GPS_LatDegrees,");
          str << F("GPS_LongDegrees,GPS_Alt");
#ifdef INCLUDE_GPS_VELOCITY
          str << F(",GPS_VelocityKnots,GPS_VelocityAngleDegrees");
#endif
          break;

        case DataSelector::Primary :
          str << F("tempBMP,pressure,altitude");
#ifdef INCLUDE_REFERENCE_ALTITUDE
          str << F(",ref.altitude");
#endif
#ifdef INCLUDE_DESCENT_VELOCITY
          str << F(",descVelocity");
#endif
          str << F(",tempThermis1");
#ifdef INCLUDE_THERMISTOR2
          str << F(",tempThermist2");
#endif
#ifdef INCLUDE_THERMISTOR3
          str << F(",tempThermist3");
#endif
          break;

        case DataSelector::Secondary :
          printCSV_SecondaryMissionHeader(str, false, false);
          break;
        case DataSelector::PrimarySecondary :
        	printCSV_ExceptTimestamp(str, DataSelector::Primary, HeaderOrContent::Header,false);
        	printCSV_SecondaryMissionHeader(str, true, false);
          break;
        case DataSelector::All:
           	printCSV_ExceptTimestamp(str, DataSelector::GPS, HeaderOrContent::Header,true);
           	printCSV_ExceptTimestamp(str, DataSelector::Primary, HeaderOrContent::Header,false);
           	printCSV_SecondaryMissionHeader(str, true, false);
          break;
        case DataSelector::AllExceptSecondary:
           	printCSV_ExceptTimestamp(str, DataSelector::GPS, HeaderOrContent::Header,true);
           	printCSV_ExceptTimestamp(str, DataSelector::Primary, HeaderOrContent::Header,false);
           	break;
        default:
          DPRINT(DBG_DIAGNOSTIC, "Unexpected DataSelector value. Terminating Program");
          DASSERT (false);
          break;
      }
      if (finalSeparator) {
        str << separator;
      }
      break;

    default:
      DPRINT(DBG_DIAGNOSTIC, "Unexpected HeaderOrContent value. Terminating Program.");
      DASSERT(false);
      break;
  }
}

void CansatRecord::printCSV(Stream& str, DataSelector select, HeaderOrContent headerOrContent) const {
	switch (headerOrContent) {
	case HeaderOrContent::Content:
		str << timestamp << separator;
		break;
	case HeaderOrContent::Header:
		str << F("timestamp,");
		break;
	default:
		DPRINT(DBG_DIAGNOSTIC, "Unexpected HeaderOrContent value. Terminating Program.");
		DASSERT(false);
		break;
	}

	printCSV_ExceptTimestamp(str, select, headerOrContent,false);
}

uint8_t CansatRecord::getBinarySize() const {
	constexpr uint8_t theSize= sizeof(timestamp) + sizeof(newGPS_Measures)
						+ 3*sizeof(uint16_t) + 4*sizeof(int32_t)
#ifdef INCLUDE_GPS_VELOCITY
	+(sizeof(uint16_t) + sizeof(int32_t))
#endif
#ifdef INCLUDE_REFERENCE_ALTITUDE
	+sizeof(int32_t)
#endif
#ifdef INCLUDE_DESCENT_VELOCITY
	+sizeof(int16_t)
#endif
#ifdef INCLUDE_THERMISTOR2
	+sizeof(int16_t)
#endif
#ifdef INCLUDE_THERMISTOR3
	+sizeof(int16_t)
#endif
	+0;
	return  theSize+getBinarySizeSecondaryMissionData();
}

bool CansatRecord::writeBinary(uint8_t* const destinationBuffer, const uint8_t bufferSize) const {
	if (bufferSize < getBinarySize()) {
		DPRINT(DBG_DIAGNOSTIC, "Insufficient buffer size to write record.");
		return 0;
	}
	uint8_t written = 0;
	uint8_t* dst = destinationBuffer;
	uint8_t remaining = bufferSize;
	written += writeBinary(dst, remaining, timestamp);
	// A. GPS data
	written += writeBinary(dst, remaining, newGPS_Measures);
	written += writeBinary(dst, remaining, (int32_t) scaleFloat(GPS_LatitudeDegrees, LatLongAccuracyScalingFactor));
		// Range: -327,68 .. 327,67 degrees
	written += writeBinary(dst, remaining, (int32_t) scaleFloat(GPS_LongitudeDegrees, LatLongAccuracyScalingFactor));
	written += writeBinary(dst, remaining, (uint32_t) scaleFloat(GPS_Altitude , AltitudeAccuracyScalingFactor));
	// 1 cm precision: range as uint16_t: 0 .. 655,35 m
	//                 range as uint32_t: 0 .. 42949672,95 m
#ifdef INCLUDE_GPS_VELOCITY
	written+=writeBinary(dst, remaining, (uint16_t) scaleFloat(GPS_VelocityKnots ,VelocityAccuracyScalingFactor));
		// range: 0 .. 655,35 m/s
	written+=writeBinary(dst, remaining, (int32_t) scaleFloat(GPS_VelocityAngleDegrees, VelocityAccuracyScalingFactor));
		// range: int16_t would be of if provided in range -180 to 180° (-327,68 .. 327,67 degrees)
		//        not if provided as 0-360°. For safety, int32_t
#endif

	// B. Primary mission data
	written += writeBinary(dst, remaining, (int16_t) scaleFloat(temperatureBMP, TemperatureAccuracyScalingFactor));
		// Range: -327,68 .. 327,67 degrees (int8_t would be a range of 25.5°C,
	    //        which is not enough.
	written += writeBinary(dst, remaining, (uint16_t) scaleFloat(pressure, PressureAccuracyScalingFactor));
	    // Range: Used range is at most 500-1500 hPa. Here: 0 .. 6553,5 hPa
	written += writeBinary(dst, remaining,(int32_t) scaleFloat(altitude, AltitudeAccuracyScalingFactor));
		// Allow for negative altitude in case of poor calibration.
		// Range: −21474836, 48 +21474836,47 m
#ifdef INCLUDE_REFERENCE_ALTITUDE
	written += writeBinary(dst, remaining,(int32_t) scaleFloat(refAltitude, AltitudeAccuracyScalingFactor));
		// Allow for negative altitude in case of poor calibration.
		// Range: −21474836, 48 +21474836,47 m
#endif
#ifdef INCLUDE_DESCENT_VELOCITY
	written+=writeBinary(dst, remaining, (int16_t) scaleFloat(descentVelocity,VelocityAccuracyScalingFactor));
	    // Range: -327,68 .. 327,67 m/s
#endif
	written += writeBinary(dst, remaining, (int16_t) scaleFloat(temperatureThermistor1, TemperatureAccuracyScalingFactor));
		// Temp. range: see temperatureBMP.
#ifdef INCLUDE_THERMISTOR2
	written += writeBinary(dst, remaining, (int16_t) scaleFloat(temperatureThermistor2, TemperatureAccuracyScalingFactor));
#endif
#ifdef INCLUDE_THERMISTOR3
	written += writeBinary(dst, remaining,(int16_t) scaleFloat(temperatureThermistor3, TemperatureAccuracyScalingFactor));
#endif
	DPRINTS(DBG, "CansatRecord::readBinary: wrote  ");
	DPRINT(DBG, written);
	DPRINTS(DBG, " bytes, before 2ndary, and ");

	written += writeBinarySecondaryMissionData(dst, remaining);
	DPRINT(DBG, written);
	DPRINTSLN(DBG, " bytes after.");

	if (written != getBinarySize()) {
		DPRINTS(DBG_DIAGNOSTIC, "CansatRecord::writeBinary: ERROR: wrote ");
		DPRINT(DBG_DIAGNOSTIC, written);
		DPRINTS(DBG_DIAGNOSTIC, " bytes, expected ");
		DPRINTLN(DBG_DIAGNOSTIC, getBinarySize());
		return false;
	} else
		return true;
}

bool CansatRecord::readBinary(const uint8_t* const sourceBuffer, const uint8_t bufferSize) {
	if (bufferSize < getBinarySize()) {
		DPRINT(DBG_DIAGNOSTIC, "Insufficient buffer size to read record.");
		return false;
	}
	uint8_t read = 0;
	const uint8_t* src = sourceBuffer;
	uint8_t remaining = bufferSize;
	uint16_t tmp_uint16;
	int16_t  tmp_int16;
	uint32_t tmp_uint32;
	int32_t	tmp_int32;

	read += readBinary(src, remaining, timestamp);

	// A. GPS data
	read += readBinary(src, remaining, newGPS_Measures);
	read += readBinary(src, remaining, tmp_int32, GPS_LatitudeDegrees, LatLongAccuracyScalingFactor);
	read += readBinary(src, remaining, tmp_int32, GPS_LongitudeDegrees, LatLongAccuracyScalingFactor);
	read += readBinary(src, remaining, tmp_uint32, GPS_Altitude, AltitudeAccuracyScalingFactor);
#ifdef INCLUDE_GPS_VELOCITY
	read += readBinary(src, remaining, tmp_uint16, GPS_VelocityKnots, VelocityAccuracyScalingFactor);
	read += readBinary(src, remaining, tmp_int32, GPS_VelocityAngleDegrees, VelocityAccuracyScalingFactor);
#endif

	// B. Primary mission data
	read += readBinary(src, remaining, tmp_int16, temperatureBMP, TemperatureAccuracyScalingFactor);
	read += readBinary(src, remaining, tmp_uint16, pressure, PressureAccuracyScalingFactor);
	read += readBinary(src, remaining, tmp_int32, altitude, AltitudeAccuracyScalingFactor);
#ifdef INCLUDE_REFERENCE_ALTITUDE
	read += readBinary(src, remaining, tmp_int32, refAltitude, AltitudeAccuracyScalingFactor);
#endif
#ifdef INCLUDE_DESCENT_VELOCITY
	read += readBinary(src, remaining, tmp_int16, descentVelocity, VelocityAccuracyScalingFactor);
#endif
	read += readBinary(src, remaining, tmp_int16, temperatureThermistor1, TemperatureAccuracyScalingFactor);
#ifdef INCLUDE_THERMISTOR2
	read += readBinary(src, remaining, tmp_int16, temperatureThermistor2, TemperatureAccuracyScalingFactor);
#endif
#ifdef INCLUDE_THERMISTOR3
	read += readBinary(src, remaining, tmp_int16, temperatureThermistor3, TemperatureAccuracyScalingFactor);
#endif
	DPRINTS(DBG, "CansatRecord::readBinary: read ");
	DPRINT(DBG, read);
	DPRINTS(DBG, " bytes, before 2ndary, and ");

	read += readBinarySecondaryMissionData(src, remaining);
	DPRINT(DBG, read);
	DPRINTSLN(DBG, " bytes after");

	if (read != getBinarySize()) {
		DPRINTS(DBG_DIAGNOSTIC, "CansatRecord::readBinary: ERROR: read ");
		DPRINT(DBG_DIAGNOSTIC, read);
		DPRINTS(DBG_DIAGNOSTIC, " bytes, expected ");
		DPRINTLN(DBG_DIAGNOSTIC, getBinarySize());
		return false;
	} else
		return true;
}
