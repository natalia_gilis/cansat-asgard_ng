/*
    AcquisitionProcess.h
    Created on: 19 janv. 2018

*/

#pragma once
#include "SdFat.h"
#include "HardwareScanner.h"
#include "elapsedMillis.h"
#include "LED_Type.h"

/** @ingroup CSPU_CansatAsgard
 *  @brief A base class installing all the logic common to any acquisition process:
    	- detect campaign start condition (but this class considers campaign is always started).
    	- read data from sensors (but this class does not actually read anything).
    	- trigger data storage (but this class does not actually store anything).
    	- blink the appropriate LEDs
    	etc.
    	It is intended to be subclassed to implement the required actions for detecting
    	campaign conditions, reading sensors and storing data. See list of methods to be
    	overridden in subclass below.
 */
class AcquisitionProcess {
  public:
     /** Constructor
     @param acquisitionPeriodInMsec The period of the acquisition cycle, in msec 
     */
    AcquisitionProcess(unsigned int acquisitionPeriodInMsec);
    virtual ~AcquisitionProcess();

    /** This is the method to call in the main loop() to have acquisition cycles performed 
     *  with the period defined by the last call to setMinDelayBetweenAcquisition().
     *  @pre The #init() method has been called */
    NON_VIRTUAL void run();

    /** Call once and only once before calling run(). 
     *  getHardwareScanner() is expected to return an initialized HardwareScanner when this method is called. 
     */
    VIRTUAL void init();

    /** Obtain the HardwareScanner initialized by the process in method initHW_Scanner()
     *  which can be overridden to create a project-specific subclass of the hardware scanner.
    */
    VIRTUAL HardwareScanner* getHardwareScanner() { return hwScanner;};

    /** Obtain a pointer to the SdFat object (fully initialized and ready to use) if any
     *  This version returns NULL and must be implemented by subclasses if relevant.
     *  @return Pointer to the SdFat object, or null if none available.
     */
    VIRTUAL SdFat* getSdFat() { return NULL;};

    /** Enquire whether the measurement campaign is started
       This default implementation always returns true but should be overridden in the
       subclasses to define a real condition to detect when the payload actually measures
       relevant data.  */
    VIRTUAL bool measurementCampaignStarted() {
      return true;
    } ;

    /** Force the start of the measurement campaign. This method is empty and should be
     *  overridden by subclasses which implement a campaign start condition.
     *  @param msg A message to send to document the reason of the start campaign.
     *  @param value An optional numeric value to document the reason of the start campaign
     */
    VIRTUAL void startMeasurementCampaign(const char* /* msg */, float /* value */=0.0f) {};
    /** Force the interruption of the measurement campaign. This method is empty and should be
     *  overridden by subclasses which implement a campaign start condition.
     */
    VIRTUAL void stopMeasurementCampaign() {};

  protected:
    /** @name Protected methods to be overridden by projects-specific subclasses.
     *  @{   */
    /** Implement in subclass to store a data record.
     *  @param campaignStarted true if the campaign is started, false
     *         if the can is in pre-flight conditions.
     */
    VIRTUAL void storeDataRecord(const bool /* campaignStarted */) {};
    
    /** Method called whenever the run() method is called. Implement to perform  
     *  housekeeping operations, if any. 
     *  Warning: this method is called very often: be sure not to perform housekeeping tasks
     *  every time, if not strictly required.
     */
    VIRTUAL void doIdle() {};
    
    /** Return a pointer to an instance of a HardwareScanner. It can be overridden by subclasses to provide
	 *  an instance of an appropriate subclass of HardwareScanner.
	 *  The object should not be initialized: its init() method will be called during the AcquisitionProcess'
	 *  initialization phase.
	 *  The object should be dynamically allocated. Ownership of the object is transferred and it will be
	 *  release when the CansatAcquisitionProcess is released.
	 */
    VIRTUAL HardwareScanner* getNewHardwareScanner();
	/** @} */  // End of group of methods to be overridden.

    /** Switch LEDs on or off.
      *  @param type Identifies the LED to adress
      *  @param status  On or Off
      */
    NON_VIRTUAL void setLED(LED_Type type, LED_State status);
  private:
    /** @name Private methods to be overridden by projects-specific subclasses.
      *  @{   */
    /** Implement in your subclass to actually acquire the data from the sensors */
    VIRTUAL void acquireDataRecord() {};

    /** Define whether this data set is worth storing. This default implementation always returns true */
    VIRTUAL bool isRecordRelevant() const { return true;}  ;

    /** Implement this method in your subclass to perform whatever generic initialization is
      *  required
      */
     virtual void initCansatProject() {};

    /** Implement this method in your subclass to perform whatever project-specific initialization is
     *  required.
     */
    virtual void initSpecificProject() {};
	/** @} */  // End of group of methods to be overridden.


    /** Configure the various output pins used to connect the LEDs controlled by the AcquisitionProcess */
    NON_VIRTUAL void initLED_Hardware();

    /** Change the state of the Heart beat LED, if it is remained unchanged long enough. 
     *  This method is called whenever the run() method is called.
     */
    void blinkHeartbeatLED ();

    unsigned int periodInMsec; /**< The acquisition period, in milliseconds. */
    elapsedMillis elapsedTime; /**< The time elapsed since the previous cycle in milliseconds */
    elapsedMillis heartbeatTimer; /**< The time elapsed since the heart beat LED was last updated. */
    LED_State heartbeatLED_State;   /**< The current state of the heart beat LED. */
    HardwareScanner* hwScanner; /**< The process' instance of the HardwareScanner */
};
