/*
   CansatRecordGoodies
*/

/**
  @brief This file gathers the implementation of all CansatRecord methods which are not used in an
  operational context. This avoid those methods to be linked in the operational software, hence
  saving memory.
*/
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "CansatRecord.h"
#define DBG 1

void CansatRecord::printPart(Stream& str, DataSelector select) const {
  switch (select) {
    case DataSelector::GPS :
      str << ENDL;
      str << (F("** GPS DATA **")) << ENDL;
      str << (F(" GPS_Measures           : ")); printCSV(str, newGPS_Measures); str << ENDL;
      str << (F(" GPS_Latitude (deg)     : ")); printCSV(str, GPS_LatitudeDegrees, false, NumDecimalPositions_GpsPosition); str << ENDL;
      str << (F(" GPS_Longitude(deg)     : ")); printCSV(str, GPS_LongitudeDegrees, false, NumDecimalPositions_GpsPosition); str << ENDL;
      str << (F(" GPS_Altitude (m)       : ")); printCSV(str, GPS_Altitude,false, NumDecimalPositions_Altitude); str << ENDL;
#ifdef INCLUDE_GPS_VELOCITY
      str << (F(" GPS_Velocity (knots)   : ")); printCSV(str, GPS_VelocityKnots, false, NumDecimalPositions_Velocity); str << ENDL;
      str << (F(" GPS_VelocityAngle (deg): ")); printCSV(str, GPS_VelocityAngleDegrees, false, NumDecimalPositions_Velocity); str << ENDL;
#endif

      break;
    case DataSelector::Primary :
      str << (F("** Primary Mission **")) << ENDL;
      str << (F(" Temp.BMP (°C)       : ")); printCSV(str, temperatureBMP, false, NumDecimalPositions_Temperature); str << ENDL;
      str << (F(" Pressure (hPa)      : ")); printCSV(str, pressure, false, NumDecimalPositions_Pressure); str << ENDL;
      str << (F(" Altitude (m)        : ")); printCSV(str, altitude, false, NumDecimalPositions_Altitude); str << ENDL;
#ifdef INCLUDE_REFERENCE_ALTITUDE
      str << (F(" Ref. altitude (m)   : ")); printCSV(str, refAltitude, false, NumDecimalPositions_Altitude); str << ENDL;
#endif
#ifdef INCLUDE_DESCENT_VELOCITY
      str << (F(" Desc. velocity (m/s): ")); printCSV(str, descentVelocity, false, NumDecimalPositions_Velocity); str << ENDL;
#endif
      str << (F(" Thermist.(°C)       : ")); printCSV(str, temperatureThermistor1, false, NumDecimalPositions_Temperature);
#ifdef INCLUDE_THERMISTOR2
      str << (F(", ")); printCSV(str, temperatureThermistor2, false, NumDecimalPositions_Temperature);
#endif
#ifdef INCLUDE_THERMISTOR3
      str << (F(", ")); printCSV(str, temperatureThermistor3, false, NumDecimalPositions_Temperature);
#endif
      str << ENDL;
      break;

    case DataSelector::Secondary :
      str << (F("** Secondary Mission **")) << ENDL;
      printSecondaryMissionData(str);
      break;

    default:
      DPRINT(DBG, "Unexpected DataSelector value. Terminating Program");
      DASSERT(false);
      break;
  }
}

void CansatRecord::print(Stream& str, DataSelector select) const {
  str << "ts (msec) = " << timestamp << ENDL;
  if (select == DataSelector::All) {
    printPart(str, DataSelector::GPS);
    printPart(str, DataSelector::Primary);
    printPart(str, DataSelector::Secondary);
  }
  else if (select == DataSelector::PrimarySecondary) {
    printPart(str, DataSelector::Primary);
    printPart(str, DataSelector::Secondary);
  }
  else if (select == DataSelector::AllExceptSecondary) {
    printPart(str, DataSelector::GPS);
    printPart(str, DataSelector::Primary);
  }
  else {
    printPart(str, select);
  }
}
