/*
 * LineStream.cpp
 */
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "LineStream.h"

#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DBG_PRINT_ALL_CHARS  0   // Print all characters received.
#define DBG_READ_FROM_STREAM 0
#define DBG_READ_LINE 0

static constexpr bool ShowBufferOverflow = true; // If true, message is sent to Serial in case of buffer overflow.

LineStream::LineStream()
: stream(NULL),
  bufferIdx(0),
  bufferSize(0),
  buffer(NULL) // 1 for the \0 and 1 to be able to assume
{

}

bool LineStream::begin(Stream& theStream, unsigned int maxLineLength) {
	bufferSize=maxLineLength+2;
	buffer = (char*) malloc(bufferSize);
	stream=&theStream;
	return (buffer != NULL);
 }

LineStream::~LineStream()
{
	if (buffer) free(buffer);
	buffer=NULL;
}

bool LineStream::readFromStream() {
	DASSERT(stream);
	// NB bufferIdx can have any value when entering this function
	DPRINTSLN(DBG_READ_FROM_STREAM,"ReadFromStream()...");
    // Receive until queue is empty or end-of-message.
	while (stream->available()) {
		DPRINTS(DBG_READ_FROM_STREAM,"bufferIdx=");
		DPRINTLN(DBG_READ_FROM_STREAM,bufferIdx);
		char c = stream->read();
		DPRINT(DBG_PRINT_ALL_CHARS,'/');
		DPRINT(DBG_PRINT_ALL_CHARS,c);
		DPRINTS(DBG_PRINT_ALL_CHARS,"=");
		DPRINT(DBG_PRINT_ALL_CHARS,(int) c);
		DPRINTLN(DBG_PRINT_ALL_CHARS,'/');
		if (bufferIdx < (bufferSize - 1)) {
			if ((c == 10) || (c == 13) || (c == '\0')) {
				// This is and end-of-message token.
				// '\n' is usually both CR  (10) and LF (13).
				if (bufferIdx != 0) {
					buffer[bufferIdx++] = '\0'; // end of string marker.
					return true;
					// stop reading as soon as a message is received: line is available
				}
				else
				{
					// just ignore end-of-string if empty buffer.
					DPRINTSLN(DBG_READ_FROM_STREAM,"Ignoring EOL");
				}
			} else {
				buffer[bufferIdx++] = c; // store character.
			}
		} else {
			// Buffer overflow: reset buffer
			if (ShowBufferOverflow) {
				Serial << "Error: buffer overflow" << ENDL;
			}
			bufferIdx = 0;
		}
	} // While available();
	return false;
}

const char* LineStream::readLine() {
	DPRINTSLN(DBG_READ_LINE, "Reading line...");
	if (readFromStream()) {
		bufferIdx=0;
		return buffer;
	} else return NULL;
}
