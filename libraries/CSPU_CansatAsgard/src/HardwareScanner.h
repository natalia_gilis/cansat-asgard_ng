/* 
 *  HardwareScanner.h
*/

#pragma once
// Double include guard: for unknown reason, in the Arduino environment,
// in some ill-defined circumstances, the pragma once directive seems to be ignored.
#ifndef __HARDWARESCANNER_H__
#define __HARDWARESCANNER_H__

#include "Arduino.h"
#include "VirtualMethods.h"
#include "CansatConfig.h" // Defines RF_ACTIVATE_API_MODE (or not).
#include "LED_Type.h"
#ifdef RF_ACTIVATE_API_MODE
#  include "CansatXBeeClient.h"
#endif

/** @ingroup CSPU_CansatAsgard
 *  @brief A generic helper class to check whether required HW is indeed present
   and responsive and to provide access to it.

   Scans the I2C bus and provides various hardware related informations.
   The following assumptions are made. Use a subclass to work on different assuptions.
   - Addresses 0x50 to 0x57 are assumed to be I2C EEPROMs
   - All I2C EEPROMs are assumed to be the same size (provided to constructor).

   @usage
   This class can be used as-is, but projects should subclass CansatHW_Scanner (see
   CansatHW_Scanner's documentation.

   Size of the object: about 20 to 30 bytes (depends on the board used). 

   @remark Currently supports the AVR board and SAMD board. 
   - For SAMD board, the HardwareScanner
     includes "Serial2.h" which causes the sketch to configure Sercom1 for use as Serial2 port.
     Would the program use SerCom1 for any other purpose, this would create a conflict.  
   - For SAMD boards, the I2C delay seems to be much longer than on Uno: takes about 0.5 sec to explore
     an I2C address, which makes the scanner quite slow. On Uno board, it's much much faster...
     Pull-ups on SCL and SDA fix the issue.

   @remark Usage of CansatConfig.h file
   This class uses the CansatConfig.h file to retrieve static information only, such as:
     - I2C addresses of components
     - Use or not of EEPROMS
     - Use of RF API mode or not.
   For more versatile choices (like the Serial port allocated to the RF or the GPS) the information
   is provided in the constructor. It does not make assumptions about the Hardware that should be
   present (but subclass CansatHW_Scanner does!)
   
   Possible optimizations:
   - Store port number of RF_Serial, rather than pointer to Serial (save 1 byte). But how to find the
     serial object just from the number?
   - Pack I2C addresses?  7 bits used on the 8. Addresses 0 to 7 are reserved so 6 bits are really necessary.
     Use 128 1 bit flags, i.e. 16 bytes for all possible addresses? This increases the size but could
     be combined with a template definition allowing to have an class supporting 8n addresses in a predefined
     range, using n+1 byte (1 for the first address).
   - Use IGNORE_EEPROMS defined in CansatConfig to strip all EEPROM related code and return constants in all
     query methods.
 */
class HardwareScanner
{
  public:
    static const unsigned int maxNbrOfI2C_Slaves = 10; /**< The maximum number of I2C slaves managed.
    														Increasing increases the memory foot-print
    														of the HardwareScanner object */
    /** @param unusedAnalogInPin The number of an unused analog input pin. It is used
     *         to read a random seed.
     */
    HardwareScanner(const byte unusedAnalogInPin=0);

    VIRTUAL ~HardwareScanner() {};

    /** Call once, before using, once the setup() function of the sketch has been entered
       (to ensure all board functions are up and running)
       Note that this call takes care of:
       	   - calling Wire.begin(), and setting the clock rate to the provided frequency
       @param firstI2C_Address  First I2C address to consider.
       @param lastI2C_Address   Last I2C address to consider.
       @param wireLibBitRateInKhz  bitRate to use to initialize the Wire library.
    */
    VIRTUAL void init(  const byte firstI2C_Address = 1,
                        const byte lastI2C_Address = 127,
                        const uint16_t wireLibBitRateInKhz = (uint16_t) 400);

    /* ----------------------------------------------------------------------------------------- */
    /** @name 1. Diagnostic methods
     *  Those methods are used to print information about the hardware status.
     *  @{
     */
    /** Print the complete diagnostic
     *  @param stream The stream on which the diagnostic must be printed.
     */
    VIRTUAL void printFullDiagnostic(Stream& stream) const;

    /**  @brief Print the list of I2C slaves detected on the I2C bus
     *   If expanding the printI2C_Diagnostic, the subclass should call the inherited method before generating
     *   any additional diagnostic.
     *   @param stream The stream on which the diagnostic must be printed.
     */
    VIRTUAL void printI2C_Diagnostic(Stream& stream) const;

    /**  @brief Print the information about the available serial ports.
     *   @param stream The stream on which the diagnostic must be printed.
     */
    NON_VIRTUAL void printSerialPortsDiagnostic(Stream& stream) const;

    /** @brief Print information about the SPI bus.
     *  Implement this method in subclass if any SPI device is used (this version is empty).
     *  The subclass should also implement checkAllSPI_Devices to collect
     *  the information.
     *  @param stream The stream on which the diagnostic must be printed.
     */
    virtual void printSPI_Diagnostic(Stream& /* stream */) const {} ;
    /** @} */ // End group of diagnostic methods

    // --------------------------------------------------------------------------------------
    // --------------------------------------------------------------------------------------
    /** @name 2. Runtime query methods
	 *  Those methods are used to dynamically check the status of particular pieces of hardware.
     *  First methods should usually not need any implementation in subclasses.
     *  @{
     */

    /** Check whether a particular I2C slave is up and running
     *  @param slaveAddress The I2C address of the slave to check.
     *  @return True if the slave is detected and responsive.
     */
    NON_VIRTUAL bool isI2C_SlaveUpAndRunning(const byte slaveAddress) const;

    /** Check whether a particular serial port is available.
     *  @param portNbr The number identifying the serial port (1=Serial, 2=Serial2 etc.)
     *  @return True if the port is available.
     */
    NON_VIRTUAL bool isSerialPortAvailable(const byte portNbr) const;

    /** Obtain a particular serial port object.
      *  @param portNbr The number identifying the serial port (1=Serial, 2=Serial2 etc.)
      *  @return A pointer to the object if the port is available, null otherwise
      */
    NON_VIRTUAL Stream* getSerialObject(byte portNbr) const;

    /** Obtain the pin number for a particular LED. This implementation always returns
      *  the built-in LED, but can be overridden by the subclass.
      *  @param type The type of the requested LED (as defined in LED_Type.h)
      *  @return The pinNumber (or 0 if no such LED).
      */
    VIRTUAL byte getLED_PinNbr(LED_Type /* type */) { return LED_BUILTIN;};

    /** Obtain the number of external EEPROM chips detected on the I2C bus.
     *  @return The number of chips.
     */
    VIRTUAL byte getNumExternalEEPROM() const;

    /** Scan the I2C bus and update results for access through isI2C_SlaveUpAndRunning() and other
     *  query methods
     *  @param firstI2C_Address  First I2C address to consider.
     *  @param lastI2C_Address   Last I2C address to consider.
     *  @param bitRateInKhz  bitRate to use to initialize the Wire library.
     */
    NON_VIRTUAL void scanI2C_Bus( const byte firstAddress = 1, const byte lastAddress = 120,
                              const byte bitRateInKhz = (byte) 400);

    /** @brief Obtain the I2C address of a particular EEPROM chip.
     *  This method should never be called if getNumExternalEEPROM() returns 0.
     *  Practically, any system using external EEPROM should subclass HardwareScanner,
     *  Implement getNumExternalEEPROM(), getExternalEEPROM_Address() and getExternalEEPROM_Size().
     *  @param EEPROM_Number The EEPROM_Number to look for. Should be >=0 and < getNumExternalEEPROM().
     *  @warning This method relies on assuptions described in the header of this file.
     *  		 Overwrite them in subclass if these assumptions are not applicable to you.
     */
    VIRTUAL_FOR_TEST byte getExternalEEPROM_I2C_Address(const byte EEPROM_Number) const ;

    /** @brief Obtain the last address in a particular EEPROM chip.
     *  This method should never be called if getNumExternalEEPROM() returns 0.
     *  Practically, any system using external EEPROM should subclass HardwareScanner,
     *  Implement getNumExternalEEPROM(), getExternalEEPROM_Address() and getExternalEEPROM_Size().
     *  @param EEPROM_Number The EEPROM_Number to look for. Should be >=0 and < getNumExternalEEPROM().
     *  @warning This method relies on assuptions described in the header of this file.
     *  		 Overwrite them in subclass if these assumptions are not applicable to you.
     */
    VIRTUAL unsigned int getExternalEEPROM_LastAddress(const byte EEPROM_Number) const ;

    /** @brief Obtain the last address of the internal EEPROM if any.
     *  (currently supports Feather M0 Express, and assumes any other board is a Genuino Uno).
     *  @return The last valid address in the EEPROM, 0 if no EEPROM available.
     */
    static int  getInternalEEPROM_LastAddress() {
#if defined(ARDUINO_ARCH_SAMD)
      // Those boards have no EEPROM.
      return 0;
#else
      return E2END;
#endif
    };

    /** @brief Obtain the default referenceVoltage for the board.
     *  (currently supports Feather M0 Express, and assumes any other board is a Genuino Uno).
     *  @return The default reference voltage.
     */
    NON_VIRTUAL static float getDefaultReferenceVoltage() {
#if defined(ARDUINO_SAMD_FEATHER_M0_EXPRESS) || defined(ARDUINO_ITSYBITSY_M0) || defined(ARDUINO_ITSYBITSY_M4)
    	// This board works on 3.3V
    	return 3.3;
#elif defined(ARDUINO_AVR_UNO)
    	// By default, assume Genuino Uno or similar
    	return 5.0;
#else
#error "Unsupported board in Hardware Scanner"
#endif
    };

    /** @brief Obtain the number of steps of the board's analog/digital converters.
     *  (currently supports Feather M0 Express, and assumes any other board is a Genuino Uno).
     *  This value can be used to define the voltage as V=ref*measure/numberOfSteps.
     *  return The number of steps (if ADC is n bits, return 2^n -1).
     */
    static int getNumADC_Steps() {
#if defined(ARDUINO_SAMD_FEATHER_M0_EXPRESS) || defined(ARDUINO_ITSYBITSY_M0) || defined(ARDUINO_ITSYBITSY_M4)
    	// This board has 12 bits ADC
    	return 4095;
#elif defined(ARDUINO_AVR_UNO)
    	// By default, assume Genuino Uno or similar with 10 bits ADC
    	return 1023;
#else
#error "Unsupported board in HardwareScanner"
#endif
    };
    /** @} */  // End of group of query methods.

  protected:
    /** Implement this method to check for your SPI devices one by one.
     *  This method is called as part of the init().
     */
    VIRTUAL void checkAllSPI_Devices() {};

    /** Return true if the chip is 64 kbytes, false otherwise (it is then assumed to be 32 kBytes)
     *  @param I2C_Address The I2C address of the chip to test */
    bool check64Kbyte(const byte I2C_Address) const;

  private:
    byte I2C_Adresses[maxNbrOfI2C_Slaves];  /**< The internal structure holding the status of I2C slaves */
    byte nbrOfI2C_Slaves;					/**< The number of I2C slaves detected */
    unsigned int I2C_EEPROM_LastAddress;	/**< The I2C address of the last external EEPROM chip */
    byte I2C_EEPROM_Flags; 					/**< A table of the detected EEPROM chips.
    											 Each byte denotes EEPROM at address 0x50+bit order. */
} ;
#endif  // file guard.
