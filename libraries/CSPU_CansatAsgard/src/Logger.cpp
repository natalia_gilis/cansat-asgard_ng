/*
   Logger.cpp
*/
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include <limits.h>
#undef USE_ASSERTIONS
#undef USE_TIMER
#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "Timer.h"
#include "Logger.h"

#define DBG_INIT_LOGGER 0
#define DBG_BUILD_FILE_NAME 0
#define DBG_FREE_SPACE 0
#define DBG_DIAGNOSTIC 1

Logger::Logger() {
  logFileName.reserve(13); // To avoid heap fragmentation.
  logFileName = "noname.txt";
}

signed char Logger::init(const String& msg,
					const char * fourCharPrefix,
                   const unsigned int requiredFreeMegs,
                   const char * extension) {
  DPRINTSLN(DBG_INIT_LOGGER, "Initializing Logger...");

  //DBG_TIMER("Logger::init");

  if (!initStorage()) return -1;
  DPRINTSLN(DBG_INIT_LOGGER, "Storage Init OK");

  if (requiredFreeMegs > 0) {
	  unsigned long freeMB = freeSpaceInMBytes();
	  DPRINTS(DBG_INIT_LOGGER, "FreeMB: Got ");
	  DPRINT(DBG_INIT_LOGGER, freeMB);
	  DPRINTS(DBG_INIT_LOGGER, ", required ");
	  DPRINTLN(DBG_INIT_LOGGER, requiredFreeMegs);

	  if (requiredFreeMegs > freeMB) {
		  DPRINTSLN(DBG_INIT_LOGGER, "Not enough space.");
		  return 1;
	  }
	  DPRINTSLN(DBG_INIT_LOGGER, "Required space OK");
  }
  else {
	  DPRINTSLN(DBG_INIT_LOGGER, "Required space not checked");
  }
  
  unsigned int counter = 1;
  do {
    buildFileName(fourCharPrefix, counter, extension);
    counter++;
  } while (fileExists(logFileName.c_str()));

  DPRINTS(DBG_INIT_LOGGER, "Name of the file: ");
  DPRINTLN(DBG_INIT_LOGGER, logFileName); // use DPRINT and not DPRINTS to avoid using the "F" macro on a string object

  if (msg.length() > 0) {
    DPRINTS(DBG_INIT_LOGGER, "writing...");
    log(msg);
    DPRINTSLN(DBG_INIT_LOGGER, "written");
    DPRINTLN(DBG_INIT_LOGGER, msg);
  }
  DPRINTSLN(DBG_INIT_LOGGER, "logger initialized.");
  return 0;
}

bool Logger::doIdle() {
  bool result = false;
  if (timeSinceMaintenance > maintenancePeriod) {
    timeSinceMaintenance = 0;
    result = performMaintenance();
  }
  return result;
}

void Logger::buildFileName(const char* fourCharPrefix, const byte number, const char * extension) {

  logFileName = fourCharPrefix;
  DASSERT(logFileName.length() == 4);

  char numStr[5];
  sprintf(numStr, "%04d", number);

  logFileName += numStr;
  logFileName += ".";
  logFileName += extension;
  DASSERT((logFileName.length()-9) <= 3);

  DPRINTS(DBG_BUILD_FILE_NAME, "Name of the file: ");
  DPRINTLN(DBG_BUILD_FILE_NAME, logFileName);
    // Do not used DPRINTSLN above to avoid wrapping the String in macro F() 
}

unsigned long Logger::freeSpaceInMBytes() {
     return (unsigned long) getFreeSpaceInMBytes();
};
    
unsigned long Logger::freeSpaceInBytes()  {
  const unsigned long overFlowLimit = ULONG_MAX / 1024;
  unsigned long  freeBytes = freeSpaceInMBytes();

  if (freeBytes >= overFlowLimit) {
    freeBytes = UINT_MAX;
  } else freeBytes *= 1024;

  DPRINTS(DBG_FREE_SPACE, "Free space bytes: ");
  DPRINTLN(DBG_FREE_SPACE, freeBytes);

  return freeBytes;
}
