/*
 * CansatHW_Scanner.h
 *
 */

#pragma once
#include "HardwareScanner.h"
#include "LED_Type.h"
#include "CansatConfig.h" // Defines RF_ACTIVATE_API_MODE (or not).
#ifdef RF_ACTIVATE_API_MODE
#  include "CansatXBeeClient.h"
#endif

/** @ingroup CSPU_CansatAsgard
 *  @brief A generic helper class to check whether the HW required for a Cansat
 *  project is indeed present and responsive and to provide access to it.

 It can be subclassed to check for SPI slaves (implement checkProjectSPI_Devices() and
 printProjectSPI_Diagnostic methods) or I2C slaves (implement checkProjectI2C_Devices()
 and printProjectI2C_Devices methods).

 @usage
 This class can be used as-is, but projects could subclass it and override the following
 methods to add project-specific detection and reporting:
 - checkProjectSPI_Devices()
 - checkProjectI2C_Devices(),
 - printProjectSPI_Diagnostic().
 - printProjectI2C_Diagnostic(),
 - printProjectDiagnostic()
 If additional initialization is required, override the init() methods
 and be sure to call CansatHW_Scanner::init() as part of your initialization.


 @remark See documentation of HardwareScanner for information about SAMD specifics.

 @remark Usage of CansatConfig.h file  The CansatHW_Scanner makes use of CansatConfig.h to
 define which hardware should be expected, and report any discrepancy.
 */
class CansatHW_Scanner: public HardwareScanner {
public:
	typedef struct Flags {
		unsigned int SD_CardReaderAvailable :1; /**< true if SD-Card reader is available. */
		unsigned int BMP_SensorAvailable :1; /**< true if BMP sensor is available. */
		unsigned int RF_InterfaceAvailable :1; /**< true if RF Serial port is available (as access
		 to the XBee module in transparent or API mode.*/
		unsigned int GPS_InterfaceAvailable :1; /**< true if GPS Serial port is available. */
		unsigned int XBeeClientAvailable :1; /**< True if the xbClient was successfully initialized */

		// NB: no flag for the sensors we cannot possibly detect.
	} Flags;

	/** @param unusedAnalogInPin The number of an unused analog input pin. It is used
	 *         to read a random seed.
	 */
	CansatHW_Scanner(const byte anUnusedAnalogInPin = unusedAnalogInPinNumber);

	virtual ~CansatHW_Scanner() {
	}
	;

	/** Overridden
	 * Call once, before using, once the setup() function of the sketch has been entered
	 * (to ensure all board functions are up and running)
	 * See details in documentation of the more complete init() method.
	 * All configuration information is taken from CansatConfig.h */
    VIRTUAL void init(  const byte firstI2C_Address = I2C_lowestAddress,
                        const byte lastI2C_Address = I2C_highestAddress,
                        const uint16_t wireLibBitRateInKhz = WireLibBitRateInKhz);


	/** Call once, before using, once the setup() function of the sketch has been entered
	 (to ensure all board functions are up and running)
	 Note that this call takes care of:
	 - calling Wire.begin(), and setting the clock rate to the provided frequency
	 - Configuring the digital pin used for the SD-Card reader, if any.
	 - Checking the presence of an SD-Card reader with provided CS, if any.
	 - initializing a CansatXBeeClient instance (if API mode is active)
	 - initializing the serial ports used for the RF, if any. (but not the GPS!).
	 - checking for the presence of a BMP280 sensor on the I2C but.
	 @param SD_CardChipSelect The digital pin used as ChipSelect for the SD-Card reader,
	 or 0 if none used.
	 @param firstI2C_Address  First I2C address to consider.
	 @param firstI2C_Address  First I2C address to consider.
	 @param lastI2C_Address   Last I2C address to consider.
	 @param RF_SerialPort The ID of the serial port to use for the RF transmitter
	 	 	 	 	 	  (0 = none, 1 = Serial1, 2 = Serial2 etc.)
	 @param GPS_SerialPort The ID of the serial port to use for the GPS module
	 	 	 	 	 	   (0 = none, 1 = Serial1, 2 = Serial2 etc.)
	 @param wireLibBitRateInKhz  bitRate to use to initialize the Wire library.
	 */
	VIRTUAL
	void init(const byte SD_CardCS,
			const byte firstI2C_Address, const byte lastI2C_Address,
			const byte RF_SerialPort, const byte GPS_SerialPort,
			const uint16_t wireLibBitRateInKhz);

    /** Print the complete diagnostic
     *  @param stream The stream on which the diagnostic must be printed.
     */
    VIRTUAL void printFullDiagnostic(Stream& stream) const;

	/**  @brief Print the list of I2C slaves detected on the I2C bus
	 *   and report missing ones. Project should complete this diagnostic
	 *   by adding information about project-specific I2C slaves by overriding
	 *   checkProjectI2C_Devices() and printProjectI2C_Diagnostic().
	 *   @param stream The stream on which the diagnostic must be printed.
	 */
	VIRTUAL void printI2C_Diagnostic(Stream& stream) const;

	/** @brief Print information about the SPI bus, collected by checkAllSPI_Devices()
	 *  method. to collect
	 *  Projects with adiditional checks to perform and report on the SPI bus
	 *  should not overridden this method, but implement checkProjectSPI_Devices()
	 *  and printProjectSPI_Diagnostic()
	 *  @param stream The stream on which the diagnostic must be printed.
	 */
	virtual void printSPI_Diagnostic(Stream& stream) const;

    /** Obtain the pin number for a particular LED.
     *  @param type The type of the requested LED (as defined in LED_Type.h)
     *  @return The pinNumber (or 0 if no such LED).
     */
    VIRTUAL byte getLED_PinNbr(LED_Type type);

#ifdef RF_ACTIVATE_API_MODE
	NON_VIRTUAL CansatXBeeClient *getRF_XBee() {
		if (flags.XBeeClientAvailable) {
			return &xbClient;
		} else
			return nullptr;
	}
#else
	/** Obtain a pointer to the Serial stream used to access the radio module.
	 *  @return A pointer to the Serial stream, or NULL if none is available.
	 */
	NON_VIRTUAL Stream* getRF_SerialObject() const {
		return (this->RF_Serial);
	}
#endif

protected:
	// --------------------------------------------------------------------------------------
	// --------------------------------------------------------------------------------------
	/** @name Methods to be overridden by projects-specific subclasses.
	 *  @{
	 */
	/** Implement this method to check for the project-specific I2C devices one by one.
	 *  This method is called as part of the init().
	 */
	VIRTUAL void checkProjectI2C_Devices() {
	}
	;

	/** Implement this method to check for the project-specific SPI devices one by one.
	 *  This method is called as part of the init() and should not chez for the SD-Card
	 *  reader with is standardly check by the CansatHW_Scanner.
	 */
	VIRTUAL void checkProjectSPI_Devices() {
	}
	;

	/**  @brief Print the list of I2C slaves detected (or unexpectedly not detected) on the I2C bus
	 *   in methode checkProjectI2C_Devices()
	 *   See reporting about BMP in method printI2C_Diagnostic() for an example of implementation.
	 *   @param stream The stream on which the diagnostic must be printed.
	 */
	VIRTUAL void printProjectI2C_Diagnostic(Stream& /* stream */) const {
	}
	;

	/**  @brief Print the list of SPI slaves detected (or unexpectedly not detected) on the SPI bus
	 *   by method checkProjectSPI_Devices().
	 *   @param stream The stream on which the diagnostic must be printed.
	 */
	VIRTUAL void printProjectSPI_Diagnostic(Stream& /* stream */) const {
	}
	;
	/**  @brief Print project-specific information, not covered by the printProjectI2C_Diagnostic()
	 *          or printProjectSPI_Diagnostic() methods.
	 *   @param stream The stream on which the diagnostic must be printed.
	 */
	VIRTUAL void printProjectDiagnostic(Stream& /* stream */) const {
	}
	;
	/** @} */  // End of group of methods to be overridden.
	// ------------------------------------------------------------------------------

	/**
	 *  This method is called as part of the init().
	 */
	VIRTUAL void checkAllSPI_Devices();

private:
    Flags flags;					/**< The flags to indicate the availability of the various elements */
	byte mSD_CardChipSelect; 		/**< The digital pin used as CS for the SD-Card reader */
	byte mRF_SerialPortNumber;		/**< The number of the serial port used for RF (0 if none). */
	byte mGPS_SerialPortNumber;		/**< The number of the serial port used for GPS (0 if none). */
#ifdef RF_ACTIVATE_API_MODE
    CansatXBeeClient xbClient;  	/**< The interface to communicate with the XBee in API mode */
#endif
    Stream *RF_Serial;				/**< The pointer to the Serial interface used for the RF
										 communication  (or NULL if none).
										 In API mode, it is used by the XBeeClient
										 and should not be used directly, in transparent mode
										 it is the interface to the RF transmitter. */
};

