/*
    Logger.h
*/
#pragma once
#include "elapsedMillis.h"
#include "CansatConfig.h"


/** @ingroup CSPU_CansatAsgard
    @brief An abstract base class for logger the classes providing file system
    storage services to the StorageManager.

    @remark It is usually a bad idea to create more than one logger on the same storage media, since objects managing
        file systems usually are large and assume there is only one instance. Subclasses could implement a check on the
        unicity of the object, or provide share the storage management object accross instances (and even allow to share
        an existing instance of this object.

  @par Design options:
  1. Do we close the file after each write or just flush it ? Is there any problem removing
  the SD_Card with open & flushed files ? Performance is excellent with both options ( < 30 msec / write, for about 60 bytes à data).
  2. Would it be better to flush or close / reopen in doIdle() rather than in log ?  Implemented but no significant improvement.
*/
class Logger {
  public :
    const unsigned long maintenancePeriod = 10000; // maintenance period in milliseconds.
    Logger();
    virtual ~Logger() {} ;

    /** Initialize the SdFat library, checks if the required amount of free space is available,
        define the name of the log file and creates it.
        The filename (8.3 file naming system) is composed as AAAAnnnn.ext where:
          AAAA is the first 4 characters of the fourCharPrefix converted to uppercases,
		  ext is the extension of the file (maximum 3 characters)
          nnnn is the smallest non null integer number (on 4 digits) such as AAAAnnnn.ext does not exist.
        Multiple inits of the same instance are supported and just start a new file (underlying storage 
        library is not reinitialized). 
        Default values for parameters are defined in CansatConfig.h
        @pre SPI port is initialized (this class does not call SPI.begin();
        @param msg  If not an empty string, it is written as first line of the file. The file is then closed.
        @param fourCharPrefix The prefix used in the file name
        @param requiredFreeMegs The number of free Mbytes to be available on the storage.
        						If 0, no check is performed on available space.
		@param extension Extension of the file that will be used. Must be maximum 3 characters long.
							Default value: "txt"
        @return 0 in case of no error.
               -1: if storage initialization could not be performed: no card, card not formatted, storage full, storage read-only,
                   attempt to initialize twice etc...
                1: if initialization was successful but required free space is not available.
    */
    virtual signed char init( const String& msg = "",
    					const char * fourCharPrefix=SD_FilesPrefix,
                        const unsigned int requiredFreeMegs = SD_RequiredFreeMegs,
                        const char * extension = SD_FilesExtension);

    /* Append data to the logfile, completed with a "\n" and flush the file */
    virtual bool log(const String& data, const bool addFinalCR = true) = 0;

    /* Return the name of the log file, if any, or an empty string if none. */
    const String& fileName() const {
      return logFileName;
    }   ;

    /*  This method should be called regularly in the loop() processing, to allow for internal maintenance
        (flushing the file to avoid data loss, etc.). After a few seconds, it should always be safe to unplug
        the board and remove the card.
        Returns true if maintenance was actually performed. */
    virtual bool doIdle();

    /**  @return the size in bytes of the logfile in bytes (does not work for files larger than 2Gb on
        8-bits boards due to unsigned long limitation).
    */
    virtual unsigned long fileSize() = 0;

    /**
        @return the free space on the storage media.  freeSpaceInBytes() returns UINT_MAX if the free space is larger than UINT_MAX bytes.
    */
    virtual unsigned long freeSpaceInBytes();

  protected:
    /**Build a valid file name based on prefix, extension and sequence number. */
    virtual void buildFileName( const char* fourCharPrefix, const byte number, const char * extension = "txt");
    /** Override this method to perform any task that must be performed regularly
        (flushing buffers etc.)
        @return true if everything ok, false otherwise.
    */
    virtual bool performMaintenance() {
      return true;
    }
    /** Initialize the underlying storage structures
        @return True if everything ok, false otherwise.
    */
    virtual bool initStorage() = 0;
    /** Return the free space on storage media. (to be implemented by subclass */
    virtual float getFreeSpaceInMBytes() = 0;
    /** Return the free space on storage media as an integer number of MB (for backward compatibility */
    virtual unsigned long freeSpaceInMBytes() ;

    /** Define whether a file exists in the underlying storage */
    virtual bool fileExists(const char* name) = 0;
  private:
    String logFileName;  /**< the name of the log file. */
    elapsedMillis timeSinceMaintenance;
};
