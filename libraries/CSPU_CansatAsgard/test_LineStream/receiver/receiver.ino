/*
   Receiver sketch for testing the LineStream class.

   Receives on serial port Serial1.

   On an Arduino UNO: Rx = RxPinOnUno, TX = TxPinOnUno (see below)
   On a Feather board: uses hardware RX-TX

   Connect Rx and Tx to Tx and Rx of emitter board.
   Do not forget to connect the grounds!

   At 115200 bauds, MaxRandomDelay=40 looses characters <hile MaxRandomDelay=30 is ok. 
   (115200 baud = 14400 char/sec = 14 char/msec.  30 msec delay = 420 characters.
   Serial buffer is supposed to be 64 bytes, so 4 msec. The other 26 are probably gained on the
   sender delay (20 msec at the time of this test). 

   Since a random delay has been added on the emitter as well, everything is ok with delays:
    Emitter 5 to 30 msec
    Receiver: 5 to 20 msec.
*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

const unsigned long SerialBaudRate = 115200;
const unsigned int MinLineLength=50;
const unsigned int MaxLineLength = 150;
const unsigned int SizeIncrement=5; 
const byte NumStringRepetitions = 7; // number of times the same string should be received.
constexpr byte RxPinOnUno = 9;
constexpr byte TxPinOnUno = 11;
constexpr byte MinRandomDelay = 3;
constexpr byte MaxRandomDelay = 20; // Delays in msec.


#include "DebugCSPU.h"
#include "LineStream.h"

#ifndef ARDUINO_SAMD_FEATHER_M0_EXPRESS
#include "SoftwareSerial.h"
SoftwareSerial Serial1(RxPinOnUno, TxPinOnUno);
#endif

LineStream lst;
unsigned int expectedLength = MinLineLength;
byte repetitionsCounter = 0;


void setup() {
  DINIT(115200);
  Serial1.begin(SerialBaudRate);
  if (!lst.begin(Serial1, MaxLineLength)) {
    Serial << "Error during LineStream.begin() ! " << ENDL;
    while (1) {
      delay(100);
    }
  }
  randomSeed(analogRead(0));
  
  Serial << "Setup OK" << ENDL;
  Serial << "This program displays lines received using a LineStream with MaxLineLength="
         << MaxLineLength << ENDL;
  Serial << "It checks the strings are indeed as sent by the emitter program " << ENDL;
  Serial << "Empty string should never be received" << ENDL;
#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
  Serial << "Using hardware serial port Serial1";
#else
  Serial << "RX = " << RxPinOnUno << ", TX=" << TxPinOnUno;
#endif
  Serial << ". Baudrate: " << SerialBaudRate << ENDL;
  Serial << "Info: \\r=" << (int) '\r' << ", \\n=" << (int) '\n' << ENDL;
  Serial << ENDL << ENDL;
}

// Check lenght and content.
// Return true if line was validated, false if it was ignored or false.
bool checkLine(const char * line) {
  int i = 0;
  bool result = true;
  // Lines starting with '-' as assumed to be OK.
  if (line[0] == '-') return false;

  while (line[i] != '\0') {
    if (line[i] != 'a' + (i % 26)) {
      Serial << " *** Error: line[" << i << "] = " << (char) line[i] << " (expected " << (char)('a' + (i % 26)) << ")" << ENDL;
      Serial << "     Line: " << line << ENDL;
      result = false;
    }
    i++;
  }
  // i is idx of \0 or \n, hence i=string length.
  if (i != (expectedLength)) {
    Serial << " *** Error: Expected length= " << expectedLength << ". Found character 0 at idx " << i << ENDL;
    result = false;
  }
  return result;
}

void loop() {
  /* THIS IS OK
  while (Serial1.available()) {
    Serial << (char) Serial1.read();
  }
  return ;
  */
  
  const char * line = lst.readLine();
  if (line) {
    Serial << "Received: " << line << " (" << strlen(line) << " chars)" << ENDL;
    if (checkLine(line)) {
      repetitionsCounter++;
      if (repetitionsCounter == NumStringRepetitions) {
        Serial << ENDL;
        repetitionsCounter = 0;
        expectedLength+=SizeIncrement;
        if (expectedLength > MaxLineLength) {
          expectedLength = MinLineLength;
        }
      } // checkLine
    } // repetitions
  } // line
  unsigned long theDelay=random(MinRandomDelay,MaxRandomDelay);
  //Serial << "Delay: "<< theDelay << " msec..." << ENDL;
  delay(theDelay);
}
