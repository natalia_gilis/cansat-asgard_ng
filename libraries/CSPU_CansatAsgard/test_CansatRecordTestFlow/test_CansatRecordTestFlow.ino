/**
 *  test_CansatRecordTestFlow.ino
 *
 *  Unit test program for classes CansatRecordTestFlow and CansatRecordExampleTestFlow
 *
 *  Requirements:
 *  	- SD Card reader (defaults to the on-board reader of Feather M0 Adalogger
 *  	- data files cstRec1.csv, cstRec2.csv, Isa2_Els.csv, Isa2Full.csv, located in folder
 *  	  testData must be in the SD-Card root directory.
 *  NB: The raw IsaTwo file is IsaTwoElsenbornOriginalComplete.csv (folder testData)
 *
 */
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CansatRecordTestFlow.h"
#include "CansatRecordExampleTestFlow.h"
#define DEBUG
#include "DebugCSPU.h"

CansatRecordTestFlow aCansatRecordFlow;
CansatRecordExampleTestFlow aSubclassFlow;
CansatRecord cstRec;
CansatRecordExample exampleRec, refRecord;
uint32_t numErrors=0;

void checkFloat(const char* msg, float val, float ref, float precision) {
	if (fabs(val - ref) > precision) {
		Serial << " Error checking " << msg << ": val=";
		Serial.print(val,5);
		Serial << ", expected=";
		Serial.print(ref, 5);
		Serial << ", precision=";
		Serial.println(precision,5);
		numErrors++;
	}
}

void pressAnyKey() {
	Serial << "Press any key + Enter to proceed..." ;
	Serial.flush();
	// remove any previous character in input queue
	while (Serial.available() > 0) {
		Serial.read();
	}
	while (Serial.available() == 0) {
		delay(300);
	}
}


void initReferenceValues(){
	refRecord.timestamp=10859;
	refRecord.GPS_LatitudeDegrees=1.23456; refRecord.GPS_LongitudeDegrees=2.12345;
	refRecord.GPS_Altitude=3.12345;
#ifdef INCLUDE_GPS_VELOCITY
	refRecord.GPS_VelocityKnots=4.12345, refRecord.GPS_VelocityAngleDegrees=5.12345;
#endif
	refRecord.temperatureBMP=18.0;
	refRecord.pressure=921.1;
	refRecord.altitude=795.2;
#ifdef INCLUDE_REFERENCE_ALTITUDE
	refRecord.refAltitude=100.1;
#endif
#ifdef INCLUDE_DESCENT_VELOCITY
	refRecord.descentVelocity=-3;
#endif
	refRecord.temperatureThermistor1=100.1;
#ifdef INCLUDE_THERMISTOR2
	refRecord.temperatureThermistor2=101.1;
#endif
#ifdef INCLUDE_THERMISTOR3
	refRecord.temperatureThermistor3=-1.1;
#endif
	refRecord.integerData=12345;
	refRecord.floatData=123.456;
}
void checkValues(CansatRecord& rec, bool checkSubClassValues) {
	if (rec.timestamp != refRecord.timestamp) {
		Serial << "Error in timestamp: val=" << rec.timestamp << ", expected=" << refRecord.timestamp << ENDL;
		numErrors++;
	}
	refRecord.timestamp+=70;
	if (rec.newGPS_Measures) {
		checkFloat("GPS_Lat",rec.GPS_LatitudeDegrees, refRecord.GPS_LatitudeDegrees, 0.000001);
		refRecord.GPS_LatitudeDegrees+=0.00001;
		checkFloat("GPS_Long",rec.GPS_LongitudeDegrees, refRecord.GPS_LongitudeDegrees, 0.000001);
		refRecord.GPS_LongitudeDegrees+=0.00001;
		checkFloat("GPS_Alt",rec.GPS_Altitude, refRecord.GPS_Altitude, 0.000001);
		refRecord.GPS_Altitude+=0.00001;
#ifdef INCLUDE_GPS_VELOCITY
		checkFloat("GPS_VelocityKnots",rec.GPS_VelocityKnots, refRecord.GPS_VelocityKnots, 0.000001);
		refRecord.GPS_VelocityKnots+=0.00001;
		checkFloat("GPS_VelocityAngle",rec.GPS_VelocityAngleDegrees, refRecord.GPS_VelocityAngleDegrees, 0.000001);
		refRecord.GPS_VelocityAngleDegrees+=0.00001;
#endif
	}
	checkFloat("TempBMP",rec.temperatureBMP, refRecord.temperatureBMP, 0.1);
	refRecord.temperatureBMP+=0.1;
	checkFloat("pressure",rec.pressure, refRecord.pressure, 0.1);
	refRecord.pressure+=0.1;
	checkFloat("altitude",rec.altitude, refRecord.altitude, 0.1);
	refRecord.altitude+=0.1;
#ifdef INCLUDE_REFERENCE_ALTITUDE
	checkFloat("refAltitude",rec.refAltitude, refRecord.refAltitude, 0.1);
	refRecord.refAltitude+=0.1;
#endif
#ifdef INCLUDE_DESCENT_VELOCITY
	checkFloat("descentVelocity",rec.descentVelocity, refRecord.descentVelocity, 0.1);
	refRecord.descentVelocity+=1;
#endif
	checkFloat("temperatureThermistor1",rec.temperatureThermistor1, refRecord.temperatureThermistor1, 0.1);
	refRecord.temperatureThermistor1+=0.1;
#ifdef INCLUDE_THERMISTOR2
	checkFloat("temperatureThermistor2",rec.temperatureThermistor2, refRecord.temperatureThermistor2, 0.1);
	refRecord.temperatureThermistor2+=0.1;
#endif
#ifdef INCLUDE_THERMISTOR3
	checkFloat("temperatureThermistor3",rec.temperatureThermistor3, refRecord.temperatureThermistor3, 0.1);
	refRecord.temperatureThermistor3-=0.1;
#endif
	if (checkSubClassValues) {
		CansatRecordExample& exRec = (CansatRecordExample&) rec;
		if (exRec.integerData != refRecord.integerData) {
			Serial << "Error in integerData: val=" << exRec.integerData << ", expected=" << refRecord.integerData << ENDL;
			numErrors++;
		}
		refRecord.integerData+=2;
		checkFloat("floatData",exRec.floatData, refRecord.floatData, 0.1);
		refRecord.floatData+=2.0f;
	}
}


void loadAndParseCSV_File(
		CansatRecordTestFlow& theFlow,
		CansatRecord& aRecord,
		const char* fileName,
		bool checkAgainstReference,
		bool checkSubClassFields)
{
	uint32_t counter=0;
	initReferenceValues();
	Serial << ENDL << "Testing with file '" << fileName << "'" ;
	if (checkSubClassFields) {
		Serial << " (checking subclass fields)";
	}
	Serial << ENDL;

	if (!theFlow.openInputFile(fileName)) {
		Serial << "Cannot read from file '" << fileName << "'. Test aborted" << ENDL;
		numErrors++;
		return;
	}
	while (theFlow.getRecord(aRecord)) {
		counter++;
		Serial << counter <<": ";
		aRecord.printCSV(Serial);
		Serial << ENDL;
		if (checkAgainstReference) {
			checkValues(aRecord, checkSubClassFields);
		}
	}
	Serial << "Reading over. Errors so far: " << numErrors << ENDL;
}

void setup() {
  DINIT(115200);

  Serial << "Init OK" << ENDL;

  Serial << ENDL;
  Serial << "Reading test files..." << ENDL;
  loadAndParseCSV_File(aCansatRecordFlow,cstRec, "cstRec1.csv", true,  false);
  loadAndParseCSV_File(aCansatRecordFlow, cstRec, "cstRec2.csv", true, false);
  loadAndParseCSV_File(aSubclassFlow, exampleRec, "cstRec2.csv", true, true);

  Serial << ENDL;
  Serial << "Reading IsaTwo actual flight file limited to 18250 lines ..." << ENDL;
  pressAnyKey();
  loadAndParseCSV_File(aCansatRecordFlow, cstRec, "Isa2_10m.csv",false, false);

  Serial << "Reading IsaTwo actual flight file (! 189 500 lines)..." << ENDL;
  pressAnyKey();
  loadAndParseCSV_File(aCansatRecordFlow, cstRec, "Isa2Full.csv",false, false);

  Serial << ENDL << ENDL;
  Serial << "End of test: ";
  if (numErrors) {
	  Serial<< " ***** " << numErrors << " errors *****." << ENDL;
  }
  else {
	  Serial << " No error." << ENDL;
  }

}

void loop() {

}
