/*
 * Test the memory usage of assertions and assertion feature. 
 * Without any assertion, just the use of Serial.print, 188 bytes of dynamic memory are used.
 * Every use of an assertion, uses somme program memory to store the location of the assertion.
 * 
 * ISSUE: preprocessor symbol __FILE_NAME__ provides the full path and symbol __BASE_FILE__ provides
 *        the file as passed to gcc, which happens to be the full path as well. As a consequence, every
 *        single assertion consumes a significant amount of program memory (typically about 100-150 bytes
 *        depending on the file path!). 
 *        A detailed search concluded that gcc does not provide the base name of the file and 
 *        there is no portable compile-time solution to strip the path.
 */
 
//#define NO_ASSERTION_AT_ALL

#ifndef NO_ASSERTION_AT_ALL
#     define __ASSERT_USE_STDERR 
      //Just to check it does not increase dynamic memory usage anymore.
#     define USE_ASSERTIONS 
#     include "AssertCSPU.h"
#endif

void setup() {
  Serial.begin(9600); 
 while (!Serial) ; 

  // put your setup code here, to run once:
 volatile byte data=1;
 Serial.println(F("testing assertions..."));
 
#ifndef NO_ASSERTION_AT_ALL
DASSERT(data==1)
DASSERT(data>0)
DASSERT(data == 5);
#endif
Serial.println(F("After assertions."));
}

void loop() { 
  // put your main code here, to run repeatedly:

}
