/* Test for the Timer utility. */

#define USE_TIMER
#include "Timer.h"

void emptyFunction() {
  DBG_TIMER("empty func");
}
void function2() {
  DBG_TIMER("func2");
  emptyFunction();
  delay(500);
}

void function1() {
  DBG_TIMER("func1");
  function2();
  delay(1000);
}

void setup() {
  Serial.begin(115200);
  while (!Serial) ;
  DBG_TIMER("setup")
  function1();
  function2();
  emptyFunction();
  Serial.println(F("End of job"));
}

void loop() {
  // put your main code here, to run repeatedly:

}
