/** Dummy file to allow editing the library in the Arduino IDE.
 * See https://arduino.stackexchange.com/questions/14189/how-to-develop-or-edit-an-arduino-library for details
 * 
 * This file can be used to check the memory footprint of the library (numbers provided for Arduino Uno):
 *  1. Undefine symbol NOT_EMPTY. Compile should give you the size of a sketch which just uses the Serial object (1860 bytes + 188 bytes dyn. memory)
 *  2. Define symbol NOT_EMPTY, but not DEBUG_CSPU, nor USE_ASSERTION: compiling will give you the size of a sketch with all debugging turned off.
 *     The size should be exactly the same (even with DASSERT, DFREE_RAM and DPRINTxx in place). 
 *  3. Defining USE_ASSERTIONS (and not DEBUG_CSPU), and using the DASSERT macro. : 2090 + 188.
 *        Second DASSERT: 2222 + 188 (+132 bytes)
 *        Third DASSERT: 2324 + 188  (+102 bytes)
 *  4. Defining DEBUG_CSPU and defining DBG_TEST to 1 (and not USE_ASSERTIONS):
 *        If DPRINTxxx is used, not DFREE_RAM: 1892 + 188 (i.e. 22 bytes, in which 4 are from the "AAAA" string). 
 *            Second DPRINTxxx: 1904 + 188, ie  +12 bytes, (4 for the string).
 *            Third  DPRINTxxx: 1916 + 188, ie. +12 bytes, (4 for the string).
 *        Each active DPRINT uses 8 bytes + length of string + 1. 
 *        If DFREE_RAM is used, not DPRINTxxx: 1998 + 192. The 4 bytes of dynamic memory are due to the link with 
 *        external variables int __heap_start, *__brkval;
 *  5. Defining DEBUG_CSPU and defining DBG_TEST to 0 (and not USE_ASSERTIONS): 1860 + 188: 0 memory foot print if the DBG_SYMBOL is set to 0,
 *     even with DPRINTxxx and DFREE_RAM in place. 
 *        
 */

//#define NOT_EMPTY

#ifdef NOT_EMPTY
#define DEBUG_CSPU
//#define USE_ASSERTIONS
#include "DebugCSPU.h"

#define DBG_TEST 1



void setup() {
  volatile int i=4;
  Serial.begin(9600);
  Serial.println(F("coucou"));  
  Serial.print(i);
  Serial.print(F("coucou"));   // Make sure the code for those methods is linked whatever the settings. 

#ifdef NOT_EMPTY
   DASSERT(i==4); 
   DASSERT(i>3); 
   DASSERT(i<3);
   DPRINTSLN(DBG_TEST, "AAA");
   DPRINTSLN(DBG_TEST, "BBB");
   DPRINTSLN(DBG_TEST, "CCC");
   DFREE_RAM(DBG_TEST);
#endif
  }

void loop(){}

#endif
