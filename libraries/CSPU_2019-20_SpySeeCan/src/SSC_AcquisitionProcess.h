/*
   SSC_AcquisitionProcess.h
   A class derived from AcquisitionProcess, which implements the SpySeeCan specific logic.
   Created on: 01 feb. 2020
*/

#pragma once
#define DBG_DIAGNOSTIC 1

#include "SSC_Config.h"

#include "AcquisitionProcess.h"
#include "SSC_StorageManager.h"
#include "SSC_HW_Scanner.h"

#include "SSC_Record.h"

#include "SSC_BMP280_Client.h"
#include "SSC_GPS_Client.h"
#include "SSC_ThermistorClient.h"

/*
  N.B.: It's not necessary to include Debug.h because SSC_Config already includes it
*/


class SSC_AcquisitionProcess : public AcquisitionProcess {
  public:
    SSC_AcquisitionProcess();
    virtual ~SSC_AcquisitionProcess() {};

    virtual void init();

    virtual SSC_HW_Scanner* getHardwareScanner();

    virtual SdFat* getSdFat() {
      return storageMgr.getSdFat();
    }

    void startMeasurementCampaign(const char* msg, float value = 0.0f);

    void stopMeasurementCampaign();

  protected:
    virtual void storeDataRecord(const bool campaignStarted);
    virtual void doIdle();


  private:
    virtual void acquireDataRecord();
    virtual void initSpecificProject();
    virtual bool measurementCampaignStarted();

    bool campaignStarted;
    float lastRecordAltitude;
    float averageSpeed;

    SSC_HW_Scanner hwScanner;
    SSC_StorageManager storageMgr;

    SSC_Record record;
    SSC_GPS_Client gps;
    SSC_BMP280_Client bmp;
    SSC_ThermistorClient thermistors;


};
