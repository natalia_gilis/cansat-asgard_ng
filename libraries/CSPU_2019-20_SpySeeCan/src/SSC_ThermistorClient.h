/*
   SSC_ThermistorClient.h
*/

#pragma once
#include "../../cansatAsgardCSPU/src/ThermistorNTCLE100E3.h"
#include "../../cansatAsgardCSPU/src/ThermistorNTCLG100E2104JB.h"
#include "../../cansatAsgardCSPU/src/ThermistorVMA320.h"
#include "SSC_Record.h"

/** @ingroup SpySeeCanCSPU
    @brief This class read the temperature we receive with our thermistors: the VMA320, the NTCLG100E3 and the NTCLE100E2.
    Wiring:      VCC - NTCLE100E3 - analog pin (defaults to A0) -  resistor - GND
                 VCC - NTCLG100E2 - analog pin (defaults to A1) -  resistor - GND
                 VMA320, looking from components side, thermistor facing up:
                  - analog pin (default=A2) to left
                  - VCC to middle
                  - Gnd to right.

*/

class SSC_ThermistorClient {
  public:
	/** Constructor
	 *  @param Vcc The voltage applied to the Thermistor+serial resistor assemblies
	 *  @param NTCLE_pin The analog pin used for the NTCLE100E3 thermistor
	 *  @param NTCLG_pin The analog pin used for the NTCLG100E2 thermistor
	 *  @param VMA_pin The analog pin used for the VMA320 thermistor
	 *  @param NTCLE_Resistor The value of the resistor in serie with the NTCLE100E3
	 *  @param NTCLE_Resistor The value of the resistor in serie with the NTCLG100E2
	 *  @param VMA320_Resistor The value of the resistor in serie with the VMA320
	 */
    SSC_ThermistorClient(float Vcc, byte NTCLE_pin=A0, byte NTCLG_pin=A1, byte VMA_pin=A2,
    					 uint32_t NTCLE_Resistor=33000, uint32_t NTCLG_Resitor=180000, uint32_t VMA_Resistor=10000);
    /** @brief  this read the temperature and write it in SSC_record and Record
     *  @param record The record to be completed with the values of temperature and pression. 
     *  @return true if the reading is succesful, false otherwise
     */
    bool readData(SSC_Record& record);

  private:
    ThermistorNTCLE100E3 thermistor1; /**< The object handling the NTCLE thermistor */
    ThermistorNTCLG100E2104JB thermistor2; /**< The object handling the NTCLE thermistor */
    ThermistorVMA320 thermistor3; /**< The object handling the VMA thermistor breakout */
};
