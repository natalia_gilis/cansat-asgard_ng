// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#ifndef ARDUINO_ARCH_SAMD
#error "Incompatible board (SAMD21 processor required)."
#endif

#include "AsyncServoWinch.h"
#include "elapsedMillis.h"
#include "TC_Timer.h"

#define USE_ASSERTIONS
#define DEBUG_CSPU
#include "DebugCSPU.h"


TC_Timer myTimer(TC_Timer::HwTimer::timerTC5);


const uint16_t maxRopeLen = 500;

const byte PWM_Pin = 9;
const uint16_t minPulseWidth = 1000;
const uint16_t maxPulseWidth = 2000;
const uint16_t motorSpeedToUse = 10;

AsyncServoWinch servo (maxRopeLen, minPulseWidth, maxPulseWidth);
class ServoWinch_Test {
  public:
    uint16_t getRopeLenFromPulseWidth(uint16_t pulseWidth){
      return servo.getRopeLenFromPulseWidth(pulseWidth);
    }
    uint16_t getPulseWidthFromRopeLen(uint16_t ropeLen){
      return servo.getPulseWidthFromRopeLen(ropeLen);
    }
};

ServoWinch_Test testHelper;

void waitForUser(char msg[] = "Press any key to continue") {
  while (Serial.available() > 0) {
    Serial.read();
  }
  Serial.println(msg);
  Serial.flush();
  while (Serial.available() == 0) {
    delay(300);
  }
  Serial.read();
}


elapsedMillis sinceTargetSet;

void setup() {
  Serial.begin(115200);
  while (!Serial) {}

  myTimer.configure(10);
  myTimer.enable();
  
  waitForUser("Press any key to start motor and fully extend (using ropeLength)");
  servo.begin(PWM_Pin, ServoWinch::ValueType::ropeLength, maxRopeLen, motorSpeedToUse);
  
  waitForUser("Press any key to go to middle (using pulse width) and overshoot by 2 cm");
  servo.setOvershootBy(20);
  servo.setTarget(ServoWinch::ValueType::pulseWidth, (minPulseWidth+maxPulseWidth)/2);
  sinceTargetSet = 0;
  while(sinceTargetSet < 20000) { // wait 20 seconds after target set
    if(!servo.isAtTarget()) Serial << "Not at target" << ENDL;
    else Serial << "Already at target for " << servo.elapsedSinceTargetReached() << " milliseconds" << ENDL;
    delay(1000);
  }

  waitForUser("Press any key to fully retract (using pulse width) and try to overshoot by 2 cm (won't actually overshoot)");
  servo.setTarget(ServoWinch::ValueType::pulseWidth, minPulseWidth);
  sinceTargetSet = 0;
  while(sinceTargetSet < 20000) { // wait 20 seconds after target set
    if(!servo.isAtTarget()) Serial << "Not at target" << ENDL;
    else Serial << "Already at target for " << servo.elapsedSinceTargetReached() << " milliseconds" << ENDL;
    delay(1000);
  }

  waitForUser("Press any key to go to middle (using rope length) and overshoot by 3 cm");
  servo.setOvershootBy(30);
  servo.setTarget(ServoWinch::ValueType::ropeLength, maxRopeLen/2);
  sinceTargetSet = 0;
  while(sinceTargetSet < 20000) { // wait 20 seconds after target set
    if(!servo.isAtTarget()) Serial << "Not at target" << ENDL;
    else Serial << "Already at target for " << servo.elapsedSinceTargetReached() << " milliseconds" << ENDL;
    delay(1000);
  }

  waitForUser("Press any key to fully retract (using rope length)");
  servo.setOvershootBy(0);
  servo.setTarget(ServoWinch::ValueType::ropeLength, 0);
  sinceTargetSet = 0;
  while(sinceTargetSet < 20000) { // wait 20 seconds after target set
    if(!servo.isAtTarget()) Serial << "Not at target" << ENDL;
    else Serial << "Already at target for " << servo.elapsedSinceTargetReached() << " milliseconds" << ENDL;
    delay(1000);
  }


  waitForUser("Press any key to turn off motor (without moving)");
  servo.end();
  sinceTargetSet = 0;
  while(sinceTargetSet < 20000) { // wait 20 seconds after target set
    if(!servo.isAtTarget()) Serial << "Not at target" << ENDL;
    else Serial << "Already at target for " << servo.elapsedSinceTargetReached() << " milliseconds" << ENDL;
    delay(1000);
  }

  waitForUser("Press any key to start motor and fully extend (using pulseWidth)");
  servo.begin(PWM_Pin, ServoWinch::ValueType::pulseWidth, maxPulseWidth, motorSpeedToUse);

  waitForUser("Press any key to turn off motor (and fully retracting it). There should also be an error message for a call to setTarget if DBG_DIAGNOSTIC is defined.");
  servo.end(true, ServoWinch::ValueType::pulseWidth, minPulseWidth);
  // the following command should have no impact and should print an error message
  servo.setTarget(ServoWinch::ValueType::pulseWidth, maxPulseWidth);

}

void loop() {
  // put your main code here, to run repeatedly:

}

void TC5_Handler (void) {
  if (myTimer.isYourInterrupt()) {
    //Serial.println("tc5");
    servo.run();
    myTimer.clearInterrupt();
  }
}
