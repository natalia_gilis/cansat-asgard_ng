/*
 * TorusSecondaryMissionController.h
 */

#pragma once
#include "TorusRecord.h"
#include "TorusServoWinch.h"
#include "elapsedMillis.h"
#include "TorusConfig.h"

/** @ingroup TorusCSPU
 *  @brief The class in charge of controlling the Torus flight, ie.:
 * 		- Computing the reference altitude, from which relative
 * 		  altitude is counted.
 * 		- Check flight safety conditions
 * 		- If in nominal descent phase, define the flight phase
 * 		  the can is currently in, depending on the phases
 * 		  defined in TorusConfig.h
 * 		- Whatever the conclusion on the safety conditions and
 * 		  flight phase, set the appropriate target for the
 * 		  servo winch.
 * 	It also fills the record with all secondary mission-related
 * 	information:
 * 		- Reference altitude
 * 		- ControllerInfo
 * 		- Flight phase
 * 		- servo-winch current target and current position.
 *  All settings are defined in TorusConfig.h, and the class
 *  ensures that flight phase does not change more than every
 *  TorusMinDelayBetweenPhaseChange msec, and servo target are
 *  not change more often than every TorusMinDelayBetweenTargetUpdate
 *  msec.
 *
 *  @par Usage
 *  @code
 *  	TorusSecondaryMissionController ctrl;
 *  	// once:
 *  	ctrl.begin(theServo);
 *  	// whenever a record is acquired:
 *  	// Preferably call in
 *  	// TorusAcquisitionProcess::acquireSecondaryMissionData()
 *  	ctrl.run(record);
 *  @endcode
 */
class TorusSecondaryMissionController {
public:
	/** Constructor */
	TorusSecondaryMissionController();

	/** Initialize the controller before use
	 * @param theServo  A reference to the ServoWinch object to use
	 * @return true if initialization is successful, false otherwise.
	 */
	bool begin(TorusServoWinch& theServo);

	/** The main entry point for the controller. It should be called
	 *  from the process acquireSecondaryMissionData() method.
	 *  @param record The Torus record, already complete with all primary
	 *  mission data.
	 */
	void run(TorusRecord& record);
	
private:
	/** Update the current flight phase if shouldUpdateFlightPhase is true.
		@param record The record containing the absolute and reference altitude
	*/
	void updateFlightPhase(TorusRecord& record);
	
	/** Update the reference altitude if necessary.
		@param record The record containing the absolute altitude.
	*/
	void updateReferenceAltitude(TorusRecord& record);
	
	/** Update the controller info if necessary.
		@param record The record containing the absolute and reference altitude, and the descent velocity.
	*/
	void updateControllerInfo(TorusRecord& record);
	
	/** Update the servomotor position if necessary.
		@param record The record containing the controller info and the flight phase
	*/
	void updateServoPosition(TorusRecord& record);

	TorusServoWinch* theServo = NULL; /**< Pointer to the servo object. */
	
	unsigned long lastCtrlInfoUpdate = 0; /**< When was the controller info last updated. */

#if 0 // Ref altitude now moved to BMP_Client
	unsigned long refAltitudeCycleBegin = 0; /**< When did the reference altitude measurement cycle begin. */
	unsigned long lastCallToUpdateReferenceAltitude = 0; /**< When was updateReferenceAltitude last called. */
	
	float refAltitude = DefaultReferenceAltitude;	 /**< The reference altitude of the cansat. */
	float minAltitudeInLastCycle = DefaultReferenceAltitude; /**< The minimum recorded altitude in the last reference altitude measurement cycle. */
	float maxAltitudeInLastCycle = DefaultReferenceAltitude; /**< The maximum recorded altitude in the last reference altitude measurement cycle. */
#endif
	
	TorusControlCode controllerInfo = TorusControlCode::NoData; /**< The current controller info. */
	uint8_t flightPhase = 15; /**< The current flight phase. */
	bool shouldUpdateFlightPhase = false; /**< Should the flight phase be updated in the next call updateFlightPhase? (should be true if in nominal descent and delay for updating has passed) */
};

