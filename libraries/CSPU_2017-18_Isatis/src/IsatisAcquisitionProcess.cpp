/*
    IsatisAcquisitionProcess.cpp

    2018.01.28: Possible memory optimisation to be evaluated: The HardwareScanner could be
    dynamically allocated and dispose of when init() completes. Benefit is not
    clear, unless memory consumption increases after init() (currently not confirmed).

*/

#include "IsatisAcquisitionProcess.h"
#include "HardwareScanner.h"
#include "IsatisConfig.h"
#include "GPY.h"
#include "GPY_Calculator.h"
#include "Adafruit_Sensor.h"
#include "Adafruit_BMP280.h"
#include <Wire.h>
#include <SPI.h>
#include "IsatisDataRecord.h"
#include "StringStream.h"
#include "Timer.h"
#include "ADT7420.h"

IsatisAcquisitionProcess::IsatisAcquisitionProcess()
  : AcquisitionProcess(	IsatisAcquisitionPeriod)
  , hwScanner(unusedAnalogInPinNumber), storageMgr(IsatisAcquisitionPeriod, IsatisCampainDuration) {
  campaignStarted = false;
  referenceAltitude = -1;
}

void IsatisAcquisitionProcess::init() {
  DPRINTSLN(DBG_INIT, "IsatisProcess::init");

  // Initialize scanner BEFORE calling AcquisitionProcess::init
  hwScanner.IsatisInit();
#ifdef PRINT_DIAGNOSTIC_AT_INIT
  hwScanner.printFullDiagnostic(Serial);
  //Serial << sizeof(hwScanner) << "," << sizeof(buffer) <<","<<sizeof(storageMgr) <<","<< sizeof(bmp) << ENDL;
  DDELAY(DBG_INIT, 1000);
#endif
  HardwareSerial * RF = hwScanner.getRF_SerialObject();
  setLED(HardwareScanner::Transmission, AcqOn);
  if (RF != NULL)
  {
    hwScanner.printFullDiagnostic(*RF);
  }
  setLED(HardwareScanner::Transmission, AcqOff);


  discardIncomingRF_Data();
  // This has been added because the RF board was ofter stopped with RX LED On, denoting
  // received data not read.

  AcquisitionProcess::init();
  String CSV_Header;
  CSV_Header.reserve(50);
  StringStream sstr(CSV_Header);
  buffer.printCSV_Header(sstr);
  storageMgr.init(&hwScanner, "ISAT", SD_CardChipSelect, SD_RequiredFreeMegs, EEPROM_KeyValue, CSV_Header);

#if (DBG_DIAGNOSTIC==1)
  float duration = ((float) storageMgr.getNumFreeEEEPROM_Records()) * IsatisAcquisitionPeriod / 1000.0 / 60.0;
  Serial << F("EEPROM can accept ") << duration << F(" min. of data. (key=0x");
  Serial.print(EEPROM_KeyValue, HEX);
  Serial << ")" << ENDL;
  if (RF != NULL)
  {
    *RF << F("EEPROM can accept ") << duration << F(" min. of data. Key=0x");
    RF->println(EEPROM_KeyValue, HEX);
  }
#endif

  discardIncomingRF_Data();
  // This has been added because the RF board was ofter stopped with RX LED On, denoting
  // received data not read.
  DPRINTSLN(DBG_INIT, "IsatisSensorsWarmupDelay...");
  delay(IsatisSensorsWarmupDelay);
#ifdef PRINT_ACQUIRED_DATA_CSV
  {
    Serial << ENDL;
    buffer.printCSV_Header(Serial);
    Serial << ENDL;
  }
#endif

  // Send header to RF and storage manager
  if (RF != NULL)
  {
    setLED(HardwareScanner::Transmission, AcqOn);
    buffer.printCSV_Header(*RF);
    RF->println();
    delay(100);
    setLED(HardwareScanner::Transmission, AcqOff);
  }
  DPRINTSLN(DBG_INIT, "IsatisProcess::init done");
}

HardwareScanner*  IsatisAcquisitionProcess::getHardwareScanner()
{
  return &hwScanner;
}

bool IsatisAcquisitionProcess::measurementCampaignStarted() {
  static byte counter = 0 ;
  if (campaignStarted == true ) {
    return true;
  }
  if (referenceAltitude == -1) {
    referenceAltitude = buffer.BMP_Altitude;
    DPRINTSLN(DBG_DIAGNOSTIC, "");
    DPRINTS(DBG_DIAGNOSTIC, " (Waiting for speed of ");
    DPRINT(DBG_DIAGNOSTIC, MinSpeedToStartCompaign);
    DPRINTS(DBG_DIAGNOSTIC, " during ");
    DPRINT(DBG_DIAGNOSTIC, NumSamplesToStartCamapaign);
    DPRINTSLN(DBG_DIAGNOSTIC, " readings)");

    DPRINTS(DBG_CAMPAIGN_STARTED, " reference altitude=");
    DPRINT(DBG_CAMPAIGN_STARTED, referenceAltitude);
    return false;
  }

  float speed = (buffer.BMP_Altitude - referenceAltitude) / IsatisAcquisitionPeriod * 1000;
  DPRINTS (DBG_CAMPAIGN_STARTED, "  speed=");
  DPRINT(DBG_CAMPAIGN_STARTED, speed);
  if (speed > MinSpeedToStartCompaign) {
    counter++;
    DPRINTS (DBG_CAMPAIGN_STARTED, ", counter=");
    DPRINT (DBG_CAMPAIGN_STARTED, counter );
  }
  else {
    counter = 0 ;
  }

  if (counter >= NumSamplesToStartCamapaign) {
    campaignStarted = true;
    GPY::setFanOn(true);
    setLED(HardwareScanner::Campaign, AcqOff);
    DPRINTSLN (DBG_DIAGNOSTIC, "");
    DPRINTSLN (DBG_DIAGNOSTIC, "=== CAMPAIGN STARTED ===");
    HardwareSerial * RF = hwScanner.getRF_SerialObject();
    if (RF != NULL)
    {
      *RF << ENDL << F("=== CAMPAIGN STARTED ===") << ENDL;
    }
    return true;
  }
  else {
    referenceAltitude = buffer.BMP_Altitude;
    DPRINTS (DBG_CAMPAIGN_STARTED, " reference altitude = ");
    DPRINT (DBG_CAMPAIGN_STARTED, referenceAltitude);
    return false;
  }
}

void IsatisAcquisitionProcess::acquireDataRecord() {
  DBG_TIMER("IsatisAcqProc::acqDataRec");
  DPRINTSLN(DBG_ACQUIRE, "IsatisAcquisitionProcess::acquireDataRecord()");

  buffer.startTimestamp = millis ();
  buffer.GPY_OutputV = GPY::getOutputV_Average( GPY_dataOutPinNbr,
                       GPY_LED_VccPinNbr,
                       GPY_NbrOfReadingsToAverage,
                       GPY_MaxDeltaInAverageSet,
                       GPY_RepeatBadQualityReadings,
                       buffer.GPY_Quality);
#ifndef USE_MINIMUM_DATARECORD
  buffer.GPY_DustDensity = GPY_Calculator::getDustDensity(buffer.GPY_OutputV, buffer.GPY_Quality);
  buffer.GPY_Voc = GPY_Calculator::Voc; // Read AFTER dust density: it is updated when calculating dust density!
  buffer.GPY_AQI = GPY_Calculator::getAQI(buffer.GPY_DustDensity);
#endif
  if (hwScanner.isI2C_SlaveUpAndRunning(I2C_BMP_SensorAddress)) {
    buffer.BMP_Pressure = (bmp.readPressure() / 100.0F) + PressureOffset;
    buffer.BMP_Altitude = bmp.readAltitude(SeaLevelPressure_HPa) + AltitudeOffset;
    //buffer.BMP_Temperature = bmp.readTemperature()+TemperatureOffset;
  }
  if (hwScanner.isI2C_SlaveUpAndRunning(I2C_ADT_SensorAddress)) {
    buffer.BMP_Temperature = ADT7420::readTemperature(I2C_ADT_SensorAddress) + TemperatureOffset;
    DPRINTLN(DBG_ACQUIRE, "Used ADT for T°");
  }
  else if (hwScanner.isI2C_SlaveUpAndRunning(I2C_BMP_SensorAddress)) {
    buffer.BMP_Temperature = bmp.readTemperature() + TemperatureOffset;
    DPRINTLN(DBG_ACQUIRE, "Used BMP for T°");
  }
  buffer.endTimestamp = millis();

#ifdef PRINT_ACQUIRED_DATA
  {
    DBG_TIMER("Serial output");
    Serial << ENDL;
    buffer.print(Serial);
  }
#endif
#ifdef PRINT_ACQUIRED_DATA_CSV
  {
    Serial << ENDL;
    DBG_TIMER("Serial output CSV");
    buffer.printCSV(Serial);
  }
#endif
  DDELAY(DBG_SLOW_DOWN_TRANSMISSION, 500);

  HardwareSerial * RF = hwScanner.getRF_SerialObject();
  setLED(HardwareScanner::Transmission, AcqOn);

  if (RF != NULL)
  {
    static byte RF_Counter = 0;
    RF_Counter++;
    DBG_TIMER("RF transmission");
    setLED(HardwareScanner::Transmission, AcqOn);
    discardIncomingRF_Data();
    // This has been added because the RF board was ofter stopped with RX LED On, denoting
    // received data not read.
    if (campaignStarted || (RF_Counter >= 50)) {
      buffer.printCSV(*RF, true); // Print slowly to avoid buffer overflow.
      RF->println();
      RF_Counter = 0;
    }
    setLED(HardwareScanner::Transmission, AcqOff);

  }

}

void IsatisAcquisitionProcess::discardIncomingRF_Data() const {
  HardwareSerial * RF = hwScanner.getRF_SerialObject();
  if (RF != NULL)
  {
    while (RF->available()) {
      RF->read(); // Discard all available incoming characters
    }
  }
}

void IsatisAcquisitionProcess::storeDataRecord(const bool campaignStarted) {
  if (campaignStarted) {
    setLED(HardwareScanner::UsingEEPROM, AcqOn); // The storage LED is managed by IsatisAcquisitionProcess::run().
    storageMgr.storeIsatisRecord( buffer, true); // during campaign, store in EEPROM
    setLED(HardwareScanner::UsingEEPROM  , AcqOff);
  } else {
    storageMgr.storeIsatisRecord( buffer, false); // Out of campaign do not store in EEPROM
  }
}

void IsatisAcquisitionProcess::initSpecificProject()  {
  DPRINTSLN(DBG_INIT, "IsatisProcess::initSpecificProject");
  if (hwScanner.isI2C_SlaveUpAndRunning(I2C_BMP_SensorAddress)) {
    bool result = bmp.begin();
    if (!result) {
      DPRINTSLN(DBG_DIAGNOSTIC, "Error initializing BMP");
    }
    result = ADT7420::init(I2C_ADT_SensorAddress);
    if (!result) {
      DPRINTSLN(DBG_DIAGNOSTIC, "Error initializing ADT");
    }
  }
  buffer.clear();

  DPRINTSLN(DBG_INIT, "End of IsatisProcess::initSpecificProject");
}

void IsatisAcquisitionProcess::doIdle()
{
  discardIncomingRF_Data();
  // This has been added because the RF board was ofter stopped with RX LED On, denoting
  // received data not read.
  storageMgr.doIdle();
}

