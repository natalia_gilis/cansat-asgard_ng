/*
   Source file for this library 
*/

//------------- Debugging control: never place in header file! -------
//#define DEBUG_CSPU              // define to enable debugging for this library
#include "DebugCSPU.h"  

#define DBG_TEST_SIMPLELIB 1
//--------------------------------------------------------------------

#include "test_simpleLib.h"
#include "Arduino.h"

const int durationOn=100;
int dummySimpleLibFunction () {
  DPRINTSLN(DBG_TEST_SIMPLELIB, "Simple library called: blinking 2 times...");
  for (int i = 0 ; i < 2; i++) {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(durationOn);
    digitalWrite(LED_BUILTIN, LOW);
    delay(durationOn);
  }
  delay(500);
  return(0);
}
