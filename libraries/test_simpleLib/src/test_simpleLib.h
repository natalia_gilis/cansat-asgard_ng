/*
 * Functions provided by source 1.
 */
#pragma once

/* 
 * Just blink the built-in LED 5 times and send a debug message on the serial line.
 */

int dummySimpleLibFunction();
