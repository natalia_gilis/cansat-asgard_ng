#pragma once
/** @ingroup CSPU_SAMD_Timers
    @brief This class encapsulates the use of Timer interrupts on SAMD processors
    (both SAMD21 and SAMD51).
    SAMD_InterruptTimers relies on library SAMD_InterruptTimers.
    SAMD_InterruptTimers can be created in any number and do not consume any
    resource until they are started. When the first one is started, the HW
    timer is created (and then shared with any subsequent instance). 

    Up to 16 instances of this class can be started simultaneously with the start() created,
    using only 1 underlying hardware timer.
*/
#ifdef ARDUINO_ARCH_SAMD

class SAMD_InterruptTimer {
  public:
    SAMD_InterruptTimer();
    ~SAMD_InterruptTimer();
    /** Start the timer: function f (void f()) will be called every period msec
        @param period The period in msec.
        @param f  A void function with no argument to be called when timer expires.
        @return true if success, false otherwise
    */
    bool start(uint32_t period, void (*f)());
    /** Update the period of a previously started timer. 
        @return true if the change was ok. */
    bool changePeriod(uint32_t period);
    /** Commpletely delete the configuration  of this timer which can then be reused just as if just created */
    void clear();
    /** returns true if the timer is enabled */
    bool isEnabled();

    /** Enables the timer (it is enabled by default). This is only possible after the time has been started  */
    void enable();
    /** Disables the timer.  This is only possible after the time has been started  */
    void disable();
    /** enables all timers.  This has no effect on timers which were not started yet.  */
    static void enableAll();
    /** disables all timers.  This has no effect on timers which were not started yet. */
    static void disableAll();
    /** enables the timer if it's currently disabled, and vice-versa.  This is only possible after the time has been started  */
    void toggle();
    /** returns the number of used timers */
    static unsigned getNumTimers();
    /** returns the number of available timers*/
    static unsigned getNumAvailableTimers();

  private:
    int libID;              /**< The internal reference for this timer in SAMD_InterruptTimers lib */
};

#endif // ARDUINO_ARCH_SAMD
