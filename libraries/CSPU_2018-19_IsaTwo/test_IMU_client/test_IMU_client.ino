/*
   test_IMU_Client.ino

   Test file for subclasses of IMU_Client.
   Wiring: just Vin-GND-SDA-SCL to one or two IMU units 
*/

#include "DebugCSPU.h"
#include "IsaTwoRecord.h"
#include "IMU_ClientLSM9DS0.h"
#include "IMU_ClientPrecisionNXP.h"
#include "Wire.h"
#include "elapsedMillis.h"

IMU_ClientLSM9DS0 imuLSM;
IMU_ClientPrecisionNXP imuNXP;
bool lsmOK, nxpOK;
IsaTwoRecord record;
elapsedMillis elapsed;

void setup() {
  DINIT(115200);
  lsmOK = nxpOK = false;
  Wire.begin();
  if (imuLSM.begin())
  {
    Serial << "LSM9DS0 detected, wiring OK, proceeding..." << ENDL;
    Serial << imuLSM.defaultNumSamplesPerRead << " samples per read" << ENDL;
    lsmOK = true;
  }
  if (imuNXP.begin())
  {
    Serial << "Precision NXP detected, wiring OK, proceeding..." << ENDL;
    Serial << imuNXP.defaultNumSamplesPerRead << " samples per read" << ENDL;
    nxpOK = true;
  }
  if (!lsmOK && !nxpOK) {
    Serial << "Oooops, no IMU unit detected... Check your wiring!" << ENDL;
  }
}

void printGravityDetails(IsaTwoRecord &record) {
    Serial << F("Accel. magnitude= ") << sqrt(record.accelRaw[0]*record.accelRaw[0]+record.accelRaw[1]*record.accelRaw[1]+record.accelRaw[2]*record.accelRaw[2]) << ENDL; 
    Serial << "Direction: XY (fromX)="<< atan2(record.accelRaw[1], record.accelRaw[0])*180.0/PI;
    Serial << "° YZ (fromY)="<< atan2(record.accelRaw[2], record.accelRaw[1])*180.0/PI;
    Serial << "° ZX (fromZ)="<< atan2(record.accelRaw[0], record.accelRaw[2])*180.0/PI << "°" << ENDL;

}

void printMagnFieldDetails(IsaTwoRecord &record) {
    Serial <<  F("Magn. Field intensity=") << sqrt(record.mag[0]*record.mag[0] +record.mag[1]*record.mag[1] +record.mag[2]*record.mag[2]) << F("µT") << ENDL;  
    Serial << "Direction: XY (fromX)="<< atan2(record.mag[1], record.mag[0])*180.0/PI;
    Serial << "° YZ (fromY)="<< atan2(record.mag[2], record.mag[1])*180.0/PI;
    Serial << "° ZX (fromZ)="<< atan2(record.mag[0], record.mag[2])*180.0/PI << "°" << ENDL;
}

void loop() {
  
  if (lsmOK) {
    record.clear();
    Serial << F("------------------From LSM9DS0");
    elapsed = 0;
    imuLSM.readData(record);
    Serial << F(" (read in ") << elapsed << F(" msecs) -------------") << ENDL;
    record.print(Serial, record.DataSelector::IMU);
    Serial.println();
    printGravityDetails(record);
    printMagnFieldDetails(record);
    Serial.println();
  }
 
  if (nxpOK) {
    record.clear();
    Serial << F("----------------From Precision NXP:");
    elapsed = 0;
    imuNXP.readData(record);
    Serial << F(" (read in ") << elapsed << F(" msecs) --------------") << ENDL;
    record.print(Serial, record.DataSelector::IMU);
    Serial.println();
    printGravityDetails(record);
    printMagnFieldDetails(record);
    Serial.println();
  }
  delay(1000);
}
