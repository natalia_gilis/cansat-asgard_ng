/* 
 * calib_MagWithMotionCal
 * 
 * Sketch to feed the MotionCal calibration application to calibrate either the
 * LSM9DS0 or the NXP_Precision IMU.
 * This sketch is used to output raw sensor data in a format that can be understoof by MotionCal 
 * from PJRC. Download the application from http://www.pjrc.com/store/prop_shield.html and make note of the 
 * magnetic offsets and transformation matrix after rotating the sensors sufficiently.
 * 
 * Based on Adafruit's ahrs_calib and ahrs_calib_usb examples.
 */

// ------ Configure here ----------------
//#define USE_NXP_PRECISION_IMU   // Select IMU here.

// ------ End of configuration ----------
#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "elapsedMillis.h"

#ifdef USE_NXP_PRECISION_IMU
#include "Adafruit_FXOS8700.h"
#include "Adafruit_FXAS21002C.h"
#else
#include "Adafruit_LSM9DS0.h"
#endif

// Create sensor instances.
#ifdef USE_NXP_PRECISION_IMU
Adafruit_FXOS8700 accelmag(0x8700A, 0x8700B);
Adafruit_FXAS21002C gyro(0x0021002C);
#else
Adafruit_LSM9DS0 lsm;   // The LSM9DS0 driver
#endif

const int ledPin = LED_BUILTIN;
int ledState = LOW;
int ledFastblinks = 0;
elapsedMillis ledMillis = 0;

void setup()
{
	bool error = false;
	DINIT(115200);
	Serial1.begin(9600);
	Serial << "Serial1 initialized" << ENDL;
	Serial1 << ENDL << ENDL << "Calibration program started..." << ENDL;

	Serial.println(F("Adafruit 9 DOF Board AHRS Calibration")); Serial.println("");
#ifdef USE_NXP_PRECISION_IMU
	Serial << "Using NXP Precision IMU...." << ENDL;
	Serial1 << "Using NXP Precision IMU...." << ENDL;
	if (!accelmag.begin(ACCEL_RANGE_2G)) {
		Serial << "Error initializing NXP accel/mag." << ENDL;
		Serial1 << "Error initializing NXP accel/mag." << ENDL;
		error = true;
	}
	if (!gyro.begin(GYRO_RANGE_250DPS))  {
		Serial << "Error initializing NXP gyro." << ENDL;
		Serial1 << "Error initializing NXP gyro." << ENDL;
		error=true;
	}
#else
	Serial << "Using LSM9DS0 IMU...." << ENDL;
	Serial1 << "Using LSM9DS0 IMU...." << ENDL;
	if (!lsm.begin()) {
		Serial << "Error initializing LSM9DS0 module" <<ENDL;
		Serial1 << "Error initializing LSM9DS0 module" <<ENDL;
		error=true;
	}
	// configure lsm
	lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_2G);
	lsm.setupMag(lsm.LSM9DS0_MAGGAIN_2GAUSS);
	lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_245DPS);
#endif	
   Serial << "WARNING: This calibration process assumes the MotionCal tool is used" << ENDL;
   Serial << "         MotionCal provides (based on data in 0.1µT/step provided by this sketch)" << ENDL;
   Serial << "         results to be applied on reading in µT (not raw readings or 0.1µT/step readings!)" <<ENDL;

	if (error) {
		Serial.println("Error during init. Aborted");
		Serial1.println("Error during init. Aborted");
		while (1);
	} else  {
		Serial.println("Init OK");
		Serial1.println("Init OK");

    // wait for user confirmation
    while (Serial.available()) ;
    Serial << " --- Press any key followed by 'return' to proceed ----";
    while (!Serial.available());
    while (Serial.available()) Serial.read();
    Serial.println();
	}
}

void loop(void)
{
#ifdef USE_NXP_PRECISION_IMU
	// NB: we are using all raw data:
	sensors_event_t unusedEvent, mag;

	/* Get a new sensor event */
	gyro.getEvent(&unusedEvent);
	accelmag.getEvent(&unusedEvent);

	Serial <<"Raw:";
	Serial << accelmag.accel_raw.x <<',' << accelmag.accel_raw.y << ',' << accelmag.accel_raw.z <<',';
	Serial << gyro.raw.x << ',' << gyro.raw.y << ',' << gyro.raw.z << ',';
	Serial << accelmag.mag_raw.x << ',' << accelmag.mag_raw.y << ',' << accelmag.mag_raw.z ;
#else

	lsm.read();
	// NB: For MotionCal, the provided values must be 0,1µT/lsb = 1 mGauss/lsb.
  // Do not change orientation of Z magnetic field for LSM. This must be done after calibration. 
	Serial.print("Raw:");
	Serial << (int16_t) lsm.accelData.x << ',' << (int16_t) lsm.accelData.y<< ',' << (int16_t) lsm.accelData.z << ',';
	Serial << (int16_t) lsm.gyroData.x << ',' << (int16_t) lsm.gyroData.y << ',' << (int16_t) lsm.gyroData.z << ',';
	Serial  << (int16_t) (lsm.magData.x *LSM9DS0_MAG_MGAUSS_2GAUSS) << ',' 
	        << (int16_t) (lsm.magData.y *LSM9DS0_MAG_MGAUSS_2GAUSS) << ',' 
	        << (int16_t) (lsm.magData.z *LSM9DS0_MAG_MGAUSS_2GAUSS);
#endif
	// WARNING: do not use Serial << ENDL; Obviously Serial.println() sends more than just a
	//          \n (most likely a \r\n and MotionCal absolutely needs it !
	Serial.println();

	// blink LED, slow normally, fast when calibration written (not implemented)
	if (ledMillis >= 1000) {
		if (ledFastblinks > 0) {
			ledFastblinks = ledFastblinks - 1;
			ledMillis -= 125;
		} else {
			ledMillis -= 1000;
		}
		if (ledState == LOW) {
			ledState = HIGH;
		} else {
			ledState = LOW;
		}
		digitalWrite(ledPin, ledState);
	}
	delay(50);
}
