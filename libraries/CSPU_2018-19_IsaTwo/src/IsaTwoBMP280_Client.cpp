/*
 * IsaTwoBMP280_Client.cpp
 */
#include "IsaTwoBMP280_Client.h"

bool IsaTwoBMP280_Client::readData(IsaTwoRecord& record){
  record.pressure = getPressure();
  record.altitude = getAltitude();
  record.temperatureBMP = getTemperature();
  return true;
}
