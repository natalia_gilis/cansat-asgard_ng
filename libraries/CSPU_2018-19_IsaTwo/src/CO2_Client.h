/*
   CO2_Client.h
*/
#pragma once
#include "IsaTwoRecord.h"

/**@ingroup IsaTwoCSPU
   Class used to read the data from the CO2 sensor
*/

class CO2_Client {

  public:
    CO2_Client(byte theAnalogInPinNumber);
    /** @brief Constructor of the class that transfers parameter's value to the private variable "analogInPinNumber"
      * @param byte theAnalogInPinNumber
      */

    bool begin(float referenceVoltage, int defaultDAC_StepNumber);
    /** @brief Transfers the function's parameters to the private variables float referenceVoltage and int DAC_StepNumber.
      * @param float referenceVoltage the reference voltage of the currently used board
      * @param int defaultDAC_StepNumber the default number of steps the currently used board's DAC has
      * @return true when the initialisation was successful, false in case of error.
      */

    void readData(IsaTwoRecord& record);
    /** @brief Reads the analog value read from the pin the CO2_sensor is plugged in and converts that value to Volts
      * @param IsaTwoRecord& record The record the value (in volts) will be stored in
      */

  private:
    byte analogInPinNumber; /**<The pin number the CO2 sensor is plugged in */
    float referenceVoltage; /**<The reference voltage of the currently used board */
    int DAC_StepNumber;     /** <The number of ADC steps */
};
