#include "config.h"

class DataManager
{
  public:
    VIRTUAL void ProcessData(const char* data);
    VIRTUAL void do1(const char* data) ;
    VIRTUAL void do2(const char* data);
} ;

class EEPROM_DataManager : public DataManager 
{
    public:
    VIRTUAL void ProcessData(const char* data);
    VIRTUAL void do1(const char* data) ;
};

