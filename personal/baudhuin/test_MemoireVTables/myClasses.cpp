#include "config.h"
#include "myClasses.h"

void DataManager::ProcessData(const char* data)
{
  DPRINTSLN(1,"EEPROM_DataManager::ProcessData");
  do1(data);
  do2(data);
}

void DataManager::do1(const char* data) {
  DPRINTSLN(1, "DataManager::do1()");
}

void DataManager::do2(const char* data) {
  DPRINTSLN(1, "DataManager::do2()");
}

//--------------

void EEPROM_DataManager::ProcessData(const char* data)
{
  DPRINTSLN(1,"EEPROM_DataManager::ProcessData");
  do1(data);
  do2(data);
}

void EEPROM_DataManager::do1(const char* data) {
  DPRINTSLN(1, "EEPROM_DataMgr::do1()");
}


