
#include "BlinkingLED.h"
#include "Arduino.h"

#define PIN_NBR 13

void BlinkingLED::begin() {
  pinMode(PIN_NBR, OUTPUT);
}

void BlinkingLED::run(){
  digitalWrite(PIN_NBR, HIGH);
  delay(1000);
  digitalWrite(PIN_NBR, LOW);
  delay(1000);
}
