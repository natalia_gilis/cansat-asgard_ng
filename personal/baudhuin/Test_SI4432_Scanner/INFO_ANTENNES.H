#ifdef NEVER

Chez Mouser: 68 antennes en stock entre 433 et 868MHz à des prix très raisonnables (entre 5 et 15€): 
https://www.mouser.be/Passive-Components/Antennas/_/N-8w0fa?P=1z0iwblZ1yzu74oZ1z0j0avZ1y94rrcZ1y94rq8Z1y9h1r9Z1y94rpwZ1y94rnuZ1y94rp5Z1y94rr9 

Les antennes souples adhésives pourraient être une bonne piste: on pourrait les coller sur la face 
extérieure ou intérieure de la cannette (ce qui imposerait qu’une des demi-canettes soit solidaire 
des circuits).  Info sur le modèle: https://www.mouser.be/molex-standalone-ant 

Il y a également des questions de connecteurs à étudier (il faut un mini-coaxe). 

Tout ce que j’ai vu comme antennes commerciales sont des antennes à bande étroite, prévues pour 
fonctionner soit à 433 soit à 868, et pour une d’entre elle, aux deux. 
Le gain de ces antennes présente un pic étroit à 433 et/ou 868 MHz. 
Si c’est pour scanner tout l’intervalle, ça ne marchera pas des masses. Il faudrait voir si 
le gain de l’antenne boudin que nous avons n’est pas aussi bon pour cet usage. 

#endif
