#include <SPI.h>
#include <RH_RF69.h>
#include "Arduino.h"

// Singleton instance of the radio
RH_RF69 rf69(6, 11);

double readings[10][10] = {0.0};
double avgReadings[10] = {0.0};
double dbmReadings[10] = {0.0}; 
const double minFrequency = 433.00;
const double maxFrequency = 866.00;
const double stepFrequency  = (maxFrequency - minFrequency) / 9;
unsigned long timestamp = 0;

double dBmConversion(double rssi){
  /*if(rssi < 218.110236){
    return 0.537671131*rssi - 124.5550595238;
  }
  else if(rssi >= 218.110236 && rssi < 227.952756){
    return 0.9417006803*rssi - 226.6380119916;
  }
  else{
    return 9999;  //Erreur
  }*/
  return rssi*0.5;
 }

void setup() 
{
  Serial.begin(9600);
  while(!Serial);
  Serial.println("START");
  pinMode(12, OUTPUT);
  digitalWrite(12, LOW);
  // manual reset
  digitalWrite(12, HIGH);
  delay(10);
  digitalWrite(12, LOW);
  delay(10);
  if (!rf69.init()){
    Serial.println("rf69 init failed");
    Serial.flush();
    while(!rf69.init()){
      timestamp = millis();
      Serial.print(".");
      delay(500);
    }
    Serial.println();
    Serial.print("Initialzing took ");
    Serial.print(millis() - timestamp);
    Serial.println(" s.");
  }
  else{
    Serial.println("Init successed");
  }
  // Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dbM (for low power module)
  rf69.setModeRx();
  Serial.print("Mode changed for : ");
  Serial.println(rf69.mode());
  rf69.setFrequency(minFrequency);
  if(!rf69.setFrequency(minFrequency)){
    Serial.println("*** Frequency not changed! ***");
    delay(1000);
    while(!rf69.setFrequency(minFrequency)){
      timestamp = millis();
      Serial.print(".");
      delay(500);
    }
    Serial.println();
    Serial.print("Initialzing took ");
    Serial.print(millis() - timestamp);
    Serial.println(" s.");
  }
}

void loop()
{
  Serial.println("Receiving...");
  rf69.setFrequency(minFrequency);
  //Readings
  for(unsigned int i = 0; i < 10; i++){
    for(unsigned int j = 0; j < 10; j++){
      readings[i][j] = rf69.rssiRead();
    }
    rf69.setFrequency(minFrequency + (stepFrequency*(i+1)));
    delay(2);
  }

  //Average
  for(unsigned int i = 0; i<10; i++){
    for(unsigned int j = 0; j<10; j++){
      avgReadings[i] += readings[i][j];
    }
    avgReadings[i] /= 10;
    dbmReadings[i] = dBmConversion(avgReadings[i]);
  }
  
  Serial.println();
  for(unsigned int i = 0; i < 10; i++){
    Serial.print("Frequency ");
    Serial.print(minFrequency + (stepFrequency*(i)));
    Serial.print(" : ");
    Serial.print(avgReadings[i]);
    Serial.print("   dbm : ");
    Serial.println(dbmReadings[i]);
  }
  delay(2);
}
