/** RECEIVER
   Copyright (c) 2009 Andrew Rapp. All rights reserved.

   This file is part of XBee-Arduino.

   XBee-Arduino is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   XBee-Arduino is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with XBee-Arduino.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <XBee.h>
#define DEBUG_CSPU
#include "DebugCSPU.h"

HardwareSerial &RF = Serial1;
/*
  This example is for Series 2 XBee
  Receives a ZB RX packet and sets a PWM value based on packet data.
  Error led is flashed if an unexpected packet is received
*/

XBee xbee = XBee();
XBeeResponse response = XBeeResponse();
// create reusable response objects for responses we expect to handle
ZBRxResponse rx = ZBRxResponse();
ModemStatusResponse msr = ModemStatusResponse();

int statusLed = 5;
int errorLed = 6;
int dataLed = 7;
int time0 = 0;

void flashLed(int pin, int times, int wait) {
  for (int i = 0; i < times; i++) {
    digitalWrite(pin, HIGH);
    delay(wait);
    digitalWrite(pin, LOW);
    if (i + 1 < times) {
      delay(wait);
    }
  }
}

void setup() {
  pinMode(statusLed, OUTPUT);
  pinMode(errorLed, OUTPUT);
  pinMode(dataLed,  OUTPUT);

  // start serial
  Serial.begin(115200);
  RF.begin(115200);
  xbee.begin(RF);
  Serial.println("===Beginning of the Tests: Transceiver side===");
  Serial.println("Checking that the LED's actually work");
  flashLed(statusLed, 3, 50);
  Serial.println("---Should have blinked---");
}

// continuously reads packets, looking for ZB Receive or Modem Status
void loop() {
  bool result = false;
  int time = millis() - time0;
  time0 = millis();
  
  xbee.readPacket();
  if (xbee.getResponse().isAvailable()) {
    // got something
    Serial.print("We got something: ");
    if (xbee.getResponse().getApiId() == ZB_RX_RESPONSE) {
      // got a zb rx packet
      Serial.println("a zb rx packet!");
      // now fill our zb rx class
      xbee.getResponse().getZBRxResponse(rx);

      if (rx.getOption() == ZB_PACKET_ACKNOWLEDGED) {
        // the sender got an ACK
        //  flashLed(statusLed, 10, 10);
        Serial.println("Sender got an ACK - good");

        uint8_t* data = rx.getData();
        uint8_t dataLength = rx.getDataLength();
        Serial << "Received " << dataLength << " bytes of data" << ENDL;
        for (int i = 0 ; i < dataLength; i++) {
          Serial << "  Byte #" << i << ": " << data[i] << "-";
        }
        result = true;
      } else {
        // we got it (obviously) but sender didn't get an ACK
        //  flashLed(errorLed, 2, 20);
        Serial.println("The sender didn't get an ACK - Problem");
      }
      // set dataLed PWM to value of the first byte in the data
    } else if (xbee.getResponse().getApiId() == MODEM_STATUS_RESPONSE) {
      xbee.getResponse().getModemStatusResponse(msr);
      // the local XBee sends this response on certain events, like association/dissociation
      Serial.println("a certain event happened association/dissociation: ");
      if (msr.getStatus() == ASSOCIATED) {
        // yay this is great.  flash led
        //  flashLed(statusLed, 10, 10);
        Serial.println("It's an assiciation - Yeay this is great!");
      } else if (msr.getStatus() == DISASSOCIATED) {
        // this is awful.. flash led to show our discontent
        //  flashLed(errorLed, 10, 10);
        Serial.println("It's a dissociation - Aïe this is awful");
      } else {
        // another status
        //   flashLed(statusLed, 5, 10);
        Serial.println("Mmm another status");
      }
    } else {
      // not something we were expecting
      //  flashLed(errorLed, 1, 25);
      Serial.println("We received somehing unexpected...");
    }
  } else if (xbee.getResponse().isError()) {
    Serial.print("Error reading packet.  Error code: ");
    Serial.println(xbee.getResponse().getErrorCode());
  }
  if(result){
    Serial << "Last receiving took: " << time << " ms" << ENDL;
  }
}
