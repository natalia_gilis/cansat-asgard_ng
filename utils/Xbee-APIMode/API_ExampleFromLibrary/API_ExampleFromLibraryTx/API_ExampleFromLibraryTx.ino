/** EMITTER
   Copyright (c) 2009 Andrew Rapp. All rights reserved.

   This file is part of XBee-Arduino.

   XBee-Arduino is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   XBee-Arduino is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with XBee-Arduino.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <XBee.h>
#define DEBUG_CSPU
#include "DebugCSPU.h"

const int RF_RxPinOnUno = 3;
const int RF_TxPinOnUno = 2;
#ifdef SIMULATE_ON_USB_SERIAL
auto &RF = Serial;
#else
#  ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
HardwareSerial &RF = Serial1;
#   else
#   include "SoftwareSerial.h"
SoftwareSerial RF(RF_RxPinOnUno, RF_TxPinOnUno);
#   endif
#endif

/*
  This example is for Series 2 XBee
  Sends a ZB TX request with the value of analogRead(pin5) and checks the status response for success
*/

// create the XBee object
XBee xbee = XBee();

uint8_t payload[2] = {0};

// SH + SL Address of receiving XBee
XBeeAddress64 addr64 = XBeeAddress64(0x0013a200, 0x41827f67);
ZBTxRequest zbTx = ZBTxRequest(addr64, payload, sizeof(payload));
ZBTxStatusResponse txStatus = ZBTxStatusResponse();

int pin5 = 0;

int statusLed = 4;
int errorLed = 5;
int counter = 0;

void flashLed(int pin, int times, int wait) {

  for (int i = 0; i < times; i++) {
    wait = wait;
    digitalWrite(pin, HIGH);
    delay(wait);
    digitalWrite(pin, LOW);
    if (i + 1 < times) {
      delay(wait);
    }
  }
}

void setup() {
  pinMode(statusLed, OUTPUT);
  pinMode(errorLed, OUTPUT);

  Serial.begin(115200);
  RF.begin(115200);
  xbee.setSerial(RF);
  Serial.println("===Beginning of the Tests: Can side===");
  Serial.println("Checking that the LED's actually work");
  flashLed(statusLed, 3, 50);
  Serial.println("---Should have blinked---");
}

void loop() {
  // break down 10-bit reading into two bytes and place in payload
  pin5 = analogRead(5);
  for (int i = 0; i < sizeof(payload); i++) {
    payload[i] = counter++;
    if (counter >= 254) {
      counter = 0;
    }
  }

  Serial << "Sending" << sizeof(payload) << "bytes" << ENDL;
  int time1 = millis();
  xbee.send(zbTx);

  // after sending a tx request, we expect a status response
  // wait up to half second for the status response
//  int time = millis();
  if (xbee.readPacket(500)) {
    //int timeStamp = millis() - time;
    //Serial << "Response took: " << timeStamp << " ms to arrive" << ENDL;


    // got a response!
  //  Serial.print("Got a response: ");
    // should be a znet tx status
    if (xbee.getResponse().getApiId() == ZB_TX_STATUS_RESPONSE) {
     // Serial.println("We got the ZB_TX_STATUS_RESPONSE...");
      xbee.getResponse().getZBTxStatusResponse(txStatus);

      // get the delivery status, the fifth byte
      if (txStatus.getDeliveryStatus() == SUCCESS) {
        // success.  time to celebrate
        Serial << "Success: status = " << txStatus.getDeliveryStatus() << ENDL;
      } else {
        // the remote XBee did not receive our packet. is it powered on?
      }
    }
  } else if (xbee.getResponse().isError()) {
    //nss.print("Error reading packet.  Error code: ");
    //nss.println(xbee.getResponse().getErrorCode());
  } else {
    // local XBee did not provide a timely TX Status Response -- should not happen
  }
  int timeProcess =  millis() - time1;
  Serial << "Whole process took: " << timeProcess << " ms" << ENDL;
  delay(1);
}
