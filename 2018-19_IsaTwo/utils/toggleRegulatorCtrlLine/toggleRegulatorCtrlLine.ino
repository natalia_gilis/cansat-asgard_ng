/*
    Utility program to toggle the regulator command line
    Only supports Feather board.
*/
#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "IsaTwoConfig.h"
#include "elapsedMillis.h"

constexpr bool toggleManually = false;  // if true, toggling when instructed on the serial line, otherwise toggle automatically.
constexpr int  autoTogglePeriod = 10; // Toggle period in seconds, when toggleManually=false.

bool regulatorOn = false;
elapsedMillis ts;

void toggleRegulator() {
  regulatorOn = (!regulatorOn);
  digitalWrite(ImageRegulatorEnablePin, (regulatorOn ? HIGH : LOW));
  Serial << "Regulator is now " << (regulatorOn ? "ON" : "OFF") << ENDL;
}

void setup() {
  DINIT(115200);
  pinMode(ImageRegulatorEnablePin, OUTPUT);
  digitalWrite(ImageRegulatorEnablePin, LOW);
  if (toggleManually) {
    Serial << "Enter any character followed by 'Enter' to toggle the regulator 'enable' line" << ENDL;
    Serial << " Enable is currently LOW" << ENDL;
    while (Serial.available()) {
      Serial.read();
    }
  } else {
    Serial << "Toggling ctrl line every " << autoTogglePeriod << " sec" << ENDL;
    ts=0;
  }
}

void loop() {
  if (toggleManually) {
    if (Serial.available()) {
      toggleRegulator();
      while (Serial.available()) {
        Serial.read();
      }
    }
  } else {
    if (ts >= autoTogglePeriod*1000) {
      toggleRegulator();
      ts=0;
    }
  }
}
