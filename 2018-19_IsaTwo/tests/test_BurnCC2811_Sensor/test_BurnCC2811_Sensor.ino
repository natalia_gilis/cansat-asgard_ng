/*
 * Utility script to turn CCS811 sensor permanently on, to perform the initial burning (48 hours required). 
 * 
 */
 
#include "CCS811_Client.h"
#include "IsaTwoRecord.h"
#include "IsaTwoConfig.h"

IsaTwoRecord record;
CCS811_Client ccs1;

constexpr unsigned long period = 500;

void setup() {
  DINIT(115200);
  analogWrite(CO2_AnalogWakePinNbr, 0); // Wake sensor
  Serial << "Sensor wake pin is now LOW" << ENDL;
  bool result = ccs1.begin(); // Intialize sensor for operation without use of the wake pin, so it remains on. 
  if (!result) {
    while (1) delay(100);
  }
  Serial << "Init OK" << ENDL;
  Serial << "Reading sensor every " << period << " msec." << ENDL;
}

void loop() {
  if (ccs1.readData(record)) {
    Serial << ENDL << millis() << ": got a value: " << record.co2 << " ppm" << ENDL;
  }
  else Serial << '.';
  delay(period);
}
